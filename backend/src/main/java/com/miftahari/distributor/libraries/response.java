package com.miftahari.distributor.libraries;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class response {
    
    public ResponseEntity<Object> generateResponse(Map<String, Object> finares, int status) {
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            // map.put("status", status.value());
            for(Map.Entry<String, Object> item : finares.entrySet()) {
                map.put(item.getKey(), item.getValue());
            }

            return new ResponseEntity<Object>(map, HttpStatus.valueOf(status));
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("error", -100);
            return new ResponseEntity<Object>(map, HttpStatus.valueOf(status)); 
        }
    }

}
