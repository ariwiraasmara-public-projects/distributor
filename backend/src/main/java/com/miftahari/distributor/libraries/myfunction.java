package com.miftahari.distributor.libraries;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.jcajce.provider.digest.BCMessageDigest;
import org.bouncycastle.util.encoders.Hex;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.text.DateFormat;  
import java.text.NumberFormat;
import java.text.SimpleDateFormat;  
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.Random;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.util.Base64;  
import java.util.Calendar;  
import jakarta.servlet.http.Cookie;

public class myfunction {
    
    public String getDate() {
        try {
            Date date = Calendar.getInstance().getTime();  
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");  
            String strDate = dateFormat.format(date);  
            // System.out.println("Converted String: " + strDate);  
            return strDate;
        }
        catch(Exception e) {
            e.printStackTrace();
            return "0000-00-00";
        }
    }

    public String getFutureDate(int count) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, count);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date futuredate = cal.getTime();
            String strFutureDate = dateFormat.format(futuredate);
            return strFutureDate.toString();
        }
        catch(Exception e) {
            e.printStackTrace();
            return "0000-00-00";
        }
    }

    public String getDateString(String format) {
        try {
            Date date = Calendar.getInstance().getTime();  
            DateFormat dateFormat = new SimpleDateFormat(format); 
            String strDate = dateFormat.format(date);  
            // System.out.println("Converted String: " + strDate);  
            return strDate;
        }
        catch(Exception e) {
            e.printStackTrace();
            return "00000000000000";
        }
    }

    public String rupiah(long val) {
        return NumberFormat.getCurrencyInstance(new Locale("id", "ID")).format(val).replace("p", "p. ");
    }

    public String formatNumber(long val) {
        return NumberFormat.getInstance(new Locale("id", "ID")).format(val);
    }

    public String random(int length) {
        try {
            String ALLOWED_CHARACTERS = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890";

            if (length < 1) {
                throw new IllegalArgumentException("Length must be greater than 0");
            }

            StringBuilder randomString = new StringBuilder(length);
            SecureRandom random = new SecureRandom();

            for (int i = 0; i < length; i++) {
                int randomIndex = random.nextInt(ALLOWED_CHARACTERS.length());
                char randomChar = ALLOWED_CHARACTERS.charAt(randomIndex);
                randomString.append(randomChar);
            }

            return randomString.toString();
        }
        catch(Exception e) {
            e.printStackTrace();
            return "__norandom__";
        }        
    }

    public String randomNumber(int length) {
        try {
            String ALLOWED_CHARACTERS = "0123456789";

            if (length <= 0) {
                throw new IllegalArgumentException("Length must be greater than 0");
            }

            StringBuilder randomString = new StringBuilder(length);
            SecureRandom random = new SecureRandom();

            for (int i = 0; i < length; i++) {
                int randomIndex = random.nextInt(ALLOWED_CHARACTERS.length());
                char randomChar = ALLOWED_CHARACTERS.charAt(randomIndex);
                randomString.append(randomChar);
            }

            return randomString.toString();
        }
        catch(Exception e) {
            e.printStackTrace();
            return "__norandom__";
        }        
    }

    public void setCookie(String[] name, String[] val, int expires) {
        try {
            Cookie[] cookie = new Cookie[name.length];
            for(int x = 0; x < name.length; x++) {
                cookie[x] = new Cookie(name[x], val[x]);
                cookie[x].setPath("/");
                cookie[x].setMaxAge(expires);
                cookie[x].setSecure(true);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    protected String key = "1234567812345678";
    protected String initVector = "1234567812345678";
    protected String algo = "AES/CBC/PKCS5PADDING";
    public String encrypt(String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
            
            Cipher cipher = Cipher.getInstance(algo);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            
            byte[] encrypted = cipher.doFinal(value.getBytes());
            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception ex) { 
            ex.printStackTrace();
        }
        return null;
    }

    public String decrypt(String encrypted) {
        try {
        IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
        
        Cipher cipher = Cipher.getInstance(algo);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
        
        byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));
        return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
