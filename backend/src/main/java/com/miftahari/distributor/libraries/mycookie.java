package com.miftahari.distributor.libraries;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.Arrays;
import java.util.Optional;

import com.miftahari.distributor.libraries.myfunction;

public class mycookie {
    
    protected myfunction fun;

    public mycookie() {
        this.fun = new myfunction();
    }

    public String getCookie(HttpServletRequest request, String key, Boolean isEncrypted) {
        try {
            Optional<String> rawval = Arrays.stream(request.getCookies())
                                                .filter(c -> key.equals(c.getName()))
                                                .map(Cookie::getValue)
                                                .findAny();

            String value = rawval.orElse("cookie_not_found");
            if(isEncrypted) { return this.fun.decrypt(value); }
            else { return value; }
        }
        catch(Exception e) {
            e.printStackTrace();
            return "__null__";
        }
    }

    public void setCookie(HttpServletResponse response, String name, String value, String path, Boolean isEncrypted) {
        try {
            Cookie cookie = new Cookie(name, null);
            if(isEncrypted) {
                cookie = new Cookie(name, this.fun.encrypt(value));
                cookie.setPath(path);
                cookie.setMaxAge(24 * 60 * 60);
                cookie.setSecure(true);
                response.addCookie(cookie);
            }
            else {
                cookie = new Cookie(name, value);
                cookie.setPath("/");
                cookie.setMaxAge(24 * 60 * 60);
                cookie.setSecure(true);
                response.addCookie(cookie);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void setCookies(HttpServletResponse response, String[] name, String[] value, String[] path) {
        try {
            Cookie[] cookie = new Cookie[name.length];
            for(int x = 0; x < name.length; x++) {
                cookie[x] = new Cookie(name[x], this.fun.encrypt(value[x]));
                cookie[x].setPath(path[x]);
                cookie[x].setMaxAge(24 * 60 * 60);
                cookie[x].setSecure(true);
                response.addCookie(cookie[x]);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteCookie(HttpServletResponse response, String name, String path) {
        try {
            Cookie cookie = new Cookie(name, null);
            cookie.setPath(path);
            cookie.setMaxAge(0);
            cookie.setSecure(false);
            response.addCookie(cookie);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteCookies(HttpServletResponse response, String[] name, String[] path) {
        try {
            Cookie[] cookie = new Cookie[name.length];
            for(int x = 0; x < name.length; x++) {
                cookie[x] = new Cookie(name[x], null);
                cookie[x].setPath(path[x]);
                cookie[x].setMaxAge(0);
                cookie[x].setSecure(false);
                response.addCookie(cookie[x]);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

}
