package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;  
// import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d1001_user")  	
public class D1001_UserModel {
    private @Getter @Setter long id_user;
    private @Getter @Setter String username;
    private @Getter @Setter String email;
    private @Getter @Setter String tlp;
    private @Getter @Setter String password;
    private @Getter @Setter int roles;
    private @Getter @Setter String token;
    private @Getter @Setter int status;
    private @Getter @Setter String created_at;
    private @Getter @Setter String updated_at;
}
