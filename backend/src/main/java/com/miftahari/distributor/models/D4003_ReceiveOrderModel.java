package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d4003_receiveorder")  	
public class D4003_ReceiveOrderModel {

    public D4003_ReceiveOrderModel(
        String id_receiveorder,
        String invoice,
        String nomor_transaksi,
        String distributor,
        long by_user,
        long barang,
        long jumlah_barang,
        String tgl_sampai,
        int is_barang_exist
    ) {}

    private @Getter @Setter String id_receiveorder;
    private @Getter @Setter String invoice;
    private @Getter @Setter String nomor_transaksi;
    private @Getter @Setter String distributor;
    private @Getter @Setter long by_user;
    private @Getter @Setter long barang;
    private @Getter @Setter long jumlah_barang;
    private @Getter @Setter String tgl_sampai;
    private @Getter @Setter int is_barang_exist; // 0 = Not Exist; 1 = Exist; 2 = On Hold, Sedang dikirim
    private @Getter @Setter List<D4002_InvoiceModel> invoice_detail;
    private @Getter @Setter List<D2003_BarangModel> barang_detail;
}
