package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;  
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d1005_personalaccesstokens")  	
public class D1005_PersonalAccessTokensModel {
    private @Getter @Setter long id_user;
    private @Getter @Setter String tokenable_type;
    private @Getter @Setter String name;
    private @Getter @Setter String token;
    private @Getter @Setter String abilities;
    private @Getter @Setter String last_used_at;
    private @Getter @Setter String expires_at;
    private @Getter @Setter String created_at;
    private @Getter @Setter String updated_at;
}
