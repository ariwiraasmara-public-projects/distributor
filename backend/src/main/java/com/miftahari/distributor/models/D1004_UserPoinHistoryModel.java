package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;  
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d1004_userpoinhistory")  	
public class D1004_UserPoinHistoryModel {
    private @Getter @Setter long id_user;
    private @Getter @Setter String deskripsi;
    private @Getter @Setter String status;
    private @Getter @Setter String tgl_history;
}
