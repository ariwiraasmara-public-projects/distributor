package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;  
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d2002_jenisbarang")  	
public class D2002_JenisBarangModel {
    private @Getter @Setter long id_jenisbarang;
    private @Getter @Setter String jenis_barang;
}
