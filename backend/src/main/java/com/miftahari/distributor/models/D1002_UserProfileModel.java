package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;  
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d1002_userprofile")  	
public class D1002_UserProfileModel {
    private @Getter @Setter long id_user;
    private @Getter @Setter String fullname;
    private @Getter @Setter String nickname;
    private @Getter @Setter String jk;
    private @Getter @Setter String alamat;
    private @Getter @Setter String foto;
}
