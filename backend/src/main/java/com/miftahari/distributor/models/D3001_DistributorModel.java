package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document; 
import com.miftahari.distributor.models.D1001_UserModel;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d3001_distributor")  	
public class D3001_DistributorModel {

    public D3001_DistributorModel(
        String id_distributor,
        long user,
        String nama_distributor,
        float longitude,
        float latitude,
        String created_at,
        String updated_at,
        String deleted_at
    ) {}

    private @Getter @Setter String id_distributor;
    private @Getter @Setter long user;
    private @Getter @Setter String nama_distributor;
    private @Getter @Setter float longitude;
    private @Getter @Setter float latitude;
    private @Getter @Setter String created_at;
    private @Getter @Setter String updated_at;
    private @Getter @Setter String deleted_at;
    private @Getter @Setter List<D1002_UserProfileModel> user_detail;
}
