package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;  
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.text.NumberFormat;
import java.util.Locale;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d1003_userwallet")  	
public class D1003_UserWalletModel {
    private @Getter @Setter long id_user;
    private @Getter @Setter float poin;
    private @Getter @Setter String updated_at;

    public String getPoin() {
        return NumberFormat.getInstance(new Locale("id", "ID")).format(poin);
    }
}
