package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;  
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d2001_merkbarang")  	
public class D2001_MerkBarangModel {
    private @Getter @Setter long id_merkbarang;
    private @Getter @Setter String merk_barang;
    private @Getter @Setter String deskripsi;
}
