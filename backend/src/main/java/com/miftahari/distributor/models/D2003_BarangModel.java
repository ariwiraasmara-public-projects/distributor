package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;  
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.text.NumberFormat;
import java.util.Locale;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d2003_barang")  	
public class D2003_BarangModel {

    public D2003_BarangModel(
        long id_barang,
        String nama_barang,
        long jenis_barang,
        long merk_barang,
        String satuan,
        long harga_beli,
        long harga_jual,
        long stok
    ) {}

    private @Getter @Setter long id_barang;
    private @Getter @Setter String nama_barang;
    private @Getter @Setter long jenis_barang;
    private @Getter @Setter long merk_barang;
    private @Getter @Setter String satuan;
    private @Getter @Setter long harga_beli;
    private @Getter @Setter long harga_jual;
    private @Getter @Setter long stok;
    private @Getter @Setter List<D2002_JenisBarangModel> jenis_barang_detail;
    private @Getter @Setter List<D2001_MerkBarangModel> merk_barang_detail;

    public String getHarga_beli_Rp() {
        return NumberFormat.getCurrencyInstance(new Locale("id", "ID")).format(harga_beli).replace("p", "p. ");
    }

    public String getHarga_jual_Rp() {
        return NumberFormat.getCurrencyInstance(new Locale("id", "ID")).format(harga_jual).replace("p", "p. ");
    }
}
