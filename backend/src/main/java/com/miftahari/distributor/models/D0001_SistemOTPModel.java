package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString
@Document(collection = "d0001_sistemotp")  	
public class D0001_SistemOTPModel {
    
    private @Getter @Setter long id_otp;
    private @Getter @Setter long id_user;
    private @Getter @Setter long kode_otp;

}
