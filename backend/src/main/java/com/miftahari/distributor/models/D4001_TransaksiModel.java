package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d4001_transaksi")  	
public class D4001_TransaksiModel {

    public D4001_TransaksiModel(
        String id_transaksi,
        String invoice,
        String nomor_transaksi,
        String distributor,
        long user,
        long barang,
        long jumlah_barang,
        int status,
        String update_at
    ) {}

    private @Getter @Setter String id_transaksi;
    private @Getter @Setter String invoice;
    private @Getter @Setter String nomor_transaksi;
    private @Getter @Setter String distributor;
    private @Getter @Setter long user;
    private @Getter @Setter long barang;
    private @Getter @Setter long jumlah_barang;
    private @Getter @Setter int status; // 0 = Nothing; 1 = Done and Bought; 2 = Pending, In Basket; 3 = Cancelled
    private @Getter @Setter String update_at;
    private @Getter @Setter List<D4002_InvoiceModel> invoice_detail;
    private @Getter @Setter List<D3001_DistributorModel> distributor_detail;
    private @Getter @Setter List<D1002_UserProfileModel> user_detail;
    private @Getter @Setter List<D2003_BarangModel> barang_detail;
}
