package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d3003_distributorstaff")  	
public class D3003_DistributorStaffModel {

    public D3003_DistributorStaffModel(
        long id_user, 
        String id_distributor, 
        String jabatan
    ) {}

    private @Getter @Setter long id_user;
    private @Getter @Setter String id_distributor;
    private @Getter @Setter String jabatan;
    private @Getter @Setter List<D1002_UserProfileModel> user_detail;
}
