package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;  
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d4002_invoice")  	
public class D4002_InvoiceModel {
    private @Getter @Setter String id_invoice;
    private @Getter @Setter String tgl_invoice;
    private @Getter @Setter String tujuan;
    private @Getter @Setter String tgl_kirim;
}
