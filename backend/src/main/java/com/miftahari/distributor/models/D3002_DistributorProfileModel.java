package com.miftahari.distributor.models;

import lombok.AllArgsConstructor;  
import lombok.Getter;  
import lombok.NoArgsConstructor;  
import lombok.Setter;  
import lombok.ToString;  
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor  
@AllArgsConstructor  
@ToString  	
@Document(collection = "d3002_distributorprofile")  	
public class D3002_DistributorProfileModel {
    private @Getter @Setter String id_distributor;
    private @Getter @Setter String deskripsi;
    private @Getter @Setter String alamat;
    private @Getter @Setter String foto;
}
