package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D4003_ReceiveOrderModel;

@RepositoryRestResource(collectionResourceRel = "d4003_receiveorder", path = "d4003_receiveorder")
public interface D4003_ReceiveOrderRepository extends MongoRepository<D4003_ReceiveOrderModel, Object> {
    
}
