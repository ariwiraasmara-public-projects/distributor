package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D3003_DistributorStaffModel;

@RepositoryRestResource(collectionResourceRel = "d3003_distributorstaff", path = "d3003_distributorstaff")
public interface D3003_DistributorStaffRepository extends MongoRepository<D3003_DistributorStaffModel, Object> {
    
}
