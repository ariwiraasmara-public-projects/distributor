package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D3002_DistributorProfileModel;

@RepositoryRestResource(collectionResourceRel = "d3002_distributorprofile", path = "d3002_distributorprofile")
public interface D3002_DistributorProfileRepository extends MongoRepository<D3002_DistributorProfileModel, Object> {
    
}
