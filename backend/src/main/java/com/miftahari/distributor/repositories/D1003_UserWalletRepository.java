package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.miftahari.distributor.models.D1003_UserWalletModel;

@RepositoryRestResource(collectionResourceRel = "d1003_userwallet", path = "d1003_userwallet")
public interface D1003_UserWalletRepository extends MongoRepository<D1003_UserWalletModel, Object> {
    
}
