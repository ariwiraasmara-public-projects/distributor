package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D1002_UserProfileModel;

@RepositoryRestResource(collectionResourceRel = "d0002_userprofile", path = "d0002_userprofile")
public interface D1002_UserProfileRepository extends MongoRepository<D1002_UserProfileModel, Object> {
    
}
