package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D4001_TransaksiModel;

@RepositoryRestResource(collectionResourceRel = "d4001_transaksi", path = "d4001_transaksi")
public interface D4001_TransaksiReporitory extends MongoRepository<D4001_TransaksiModel, Object> {
    
}
