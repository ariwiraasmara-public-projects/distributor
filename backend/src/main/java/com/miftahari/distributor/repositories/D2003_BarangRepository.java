package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D2003_BarangModel;

@RepositoryRestResource(collectionResourceRel = "d2003_barang", path = "d2003_barang")
public interface D2003_BarangRepository extends MongoRepository<D2003_BarangModel, Object> {
    
}
