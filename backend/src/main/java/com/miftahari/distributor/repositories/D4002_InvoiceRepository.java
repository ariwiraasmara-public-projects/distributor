package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D4002_InvoiceModel;

@RepositoryRestResource(collectionResourceRel = "d4002_invoice", path = "d4002_invoice")
public interface D4002_InvoiceRepository extends MongoRepository<D4002_InvoiceModel, Object> {
    
}
