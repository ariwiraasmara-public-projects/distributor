package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D1004_UserPoinHistoryModel;

@RepositoryRestResource(collectionResourceRel = "d1004_userpoinhistory", path = "d1004_userpoinhistory")
public interface D1004_UserPoinHistoryRepository extends MongoRepository<D1004_UserPoinHistoryModel, Object> {
    
}
