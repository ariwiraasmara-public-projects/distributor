package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D3001_DistributorModel;

@RepositoryRestResource(collectionResourceRel = "d3001_distributor", path = "d3001_distributor")
public interface D3001_DistributorRepository extends MongoRepository<D3001_DistributorModel, Object> {
    
}
