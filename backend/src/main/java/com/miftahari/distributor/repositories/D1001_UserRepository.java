package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.miftahari.distributor.models.D1001_UserModel;

@RepositoryRestResource(collectionResourceRel = "d1001_user", path = "d1001_user")
public interface D1001_UserRepository extends MongoRepository<D1001_UserModel, Object> {
    
}
