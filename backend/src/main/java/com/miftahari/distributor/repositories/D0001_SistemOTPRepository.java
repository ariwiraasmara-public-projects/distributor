package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D0001_SistemOTPModel;

@RepositoryRestResource(collectionResourceRel = "d0001_sistemotp", path = "d0001_sistemotp")
public interface D0001_SistemOTPRepository extends MongoRepository<D0001_SistemOTPModel, Object> {
    
}
