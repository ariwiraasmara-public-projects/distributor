package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D2002_JenisBarangModel;

@RepositoryRestResource(collectionResourceRel = "d2002_jenisbarang", path = "d2002_jenisbarang")
public interface D2002_JenisBarangRepository extends MongoRepository<D2002_JenisBarangModel, Object> {
    
}
