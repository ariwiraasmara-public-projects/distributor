package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D1005_PersonalAccessTokensModel;

@RepositoryRestResource(collectionResourceRel = "d1005_personalaccesstokens", path = "d1005_personalaccesstokens")
public interface D1005_PersonalAccessTokensRepository extends MongoRepository<D1005_PersonalAccessTokensModel, Object> {
    
}
