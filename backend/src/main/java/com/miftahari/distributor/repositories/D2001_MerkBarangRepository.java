package com.miftahari.distributor.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.miftahari.distributor.models.D2001_MerkBarangModel;

@RepositoryRestResource(collectionResourceRel = "d2001_merkbarang", path = "d2001_merkbarang")
public interface D2001_MerkBarangRepository extends MongoRepository<D2001_MerkBarangModel, Object> {
    
}
