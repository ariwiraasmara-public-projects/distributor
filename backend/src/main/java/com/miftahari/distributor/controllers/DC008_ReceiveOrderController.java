package com.miftahari.distributor.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miftahari.distributor.services.D4003_ReceiveOrderService;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/receiveorder")
public class DC008_ReceiveOrderController {
    
    @Autowired
    private D4003_ReceiveOrderService service;

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable String id) throws Exception {
        return this.service.get(id);
    }

    @GetMapping("/all/{id}")
    public Object getAll(@PathVariable String id) throws Exception {
        return this.service.getAll(id);
    }

    @PutMapping("/update")
    public Object getAll(@RequestParam String id_receiveorder, @RequestParam String nomor_transaksi) throws Exception {
        return this.service.update(id_receiveorder, nomor_transaksi);
    }
}
