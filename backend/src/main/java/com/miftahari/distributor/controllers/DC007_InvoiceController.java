package com.miftahari.distributor.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miftahari.distributor.services.D4002_InvoiceService;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/invoice")
public class DC007_InvoiceController {
    
    @Autowired
    private D4002_InvoiceService service;

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable String id) throws Exception {
        return this.service.get(id);
    }

    @GetMapping("/all/{id}")
    public Object getAll(@PathVariable String id) throws Exception {
        return this.service.getAll(id);
    }

    @PutMapping("/update/{id}")
    public Object update(@PathVariable String id, @RequestParam String tujuan, @RequestParam String tgl_kirim) throws Exception {
        return this.service.update(id, tujuan, tgl_kirim);
    }

    @DeleteMapping("/delete/{id}")
    public Object delete(@PathVariable String id) throws Exception {
        return this.service.delete(id);
    }

}
