package com.miftahari.distributor.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miftahari.distributor.models.D1001_UserModel;
import com.miftahari.distributor.models.D3001_DistributorModel;
import com.miftahari.distributor.models.D4001_TransaksiModel;
import com.miftahari.distributor.services.D4001_TransaksiService;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/transaksi")
public class DC006_TransaksiController {
    
    @Autowired
    private D4001_TransaksiService service;

    @GetMapping("/{nomor_transaksi}")
    public ResponseEntity<Object> get(@RequestParam String nomor_transaksi) throws Exception {
        return this.service.get(nomor_transaksi);
    }

    @GetMapping("/all/{id}")
    public ResponseEntity<Object> getAll(@PathVariable String id) throws Exception {
        return this.service.getAll(id);
    }

    @PostMapping("/save/tobasket")
    public ResponseEntity<Object> toBasket(@RequestBody D4001_TransaksiModel model) throws Exception {
        return this.service.toBasket(model);
    }

    @PutMapping("/save/{id}")
    public ResponseEntity<Object> setTransaction(
        @PathVariable String id, 
        @RequestParam long id_user,
        @RequestParam String id_transaksi
    ) throws Exception {
        return this.service.setTransaction(id, id_user, id_transaksi);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> update(
        @PathVariable String id, 
        @RequestParam String tujuan, 
        @RequestParam String tgl_kirim
    ) throws Exception {
        return this.service.update(id, tujuan, tgl_kirim);
    }

    /* 
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) throws Exception {
        return this.service.deleteOne(id);
    }

    @DeleteMapping("/delete/all/{id}")
    public ResponseEntity<Object> deleteAll(@PathVariable String id) throws Exception {
        return this.service.delete(id);
    }
    */
}
