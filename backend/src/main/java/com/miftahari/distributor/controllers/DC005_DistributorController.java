package com.miftahari.distributor.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miftahari.distributor.models.D1001_UserModel;
import com.miftahari.distributor.models.D3001_DistributorModel;
import com.miftahari.distributor.services.D1001_UserService;
import com.miftahari.distributor.services.D3001_DistributorService;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/distributor")
public class DC005_DistributorController {
    
    @Autowired
    private D3001_DistributorService service;

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable String id) throws Exception {
        return this.service.get(id);
    }

    @GetMapping("/staff/{id}")
    public ResponseEntity<Object> getStaff(@PathVariable String id) throws Exception {
        return this.service.getAllStaff(id);
    }

    @GetMapping("/all")
    public Object getAll() throws Exception {
        return this.service.getAll();
    }

    @PostMapping("/save")
    public ResponseEntity<Object> create(
        @RequestParam String username,
        @RequestParam String email,
        @RequestParam String tlp,
        @RequestParam String password,
        @RequestParam String nama_distributor) throws Exception {
        return this.service.save(nama_distributor, username, email, tlp, password);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> update(
        @PathVariable String id, 
        @RequestParam String nama_distributor,
        @RequestParam float longitude,
        @RequestParam float latitude,
        @RequestParam String deskripsi,
        @RequestParam String alamat,
        @RequestParam String foto
        ) throws Exception {
        return this.service.update(id, nama_distributor, longitude, latitude, deskripsi, alamat, foto);
    }

    @PutMapping("/softdelete/{id}")
    public ResponseEntity<Object> softDelete(@PathVariable String id) throws Exception {
        return this.service.softDelete(id);
    }

    @DeleteMapping("/harddelete/{id}")
    public ResponseEntity<Object> hardDelete(@PathVariable String id) throws Exception {
        return this.service.hardDelete(id);
    }

}
