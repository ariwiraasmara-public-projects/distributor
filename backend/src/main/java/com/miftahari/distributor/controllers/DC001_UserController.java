package com.miftahari.distributor.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.miftahari.distributor.models.D1001_UserModel;
import com.miftahari.distributor.services.D1001_UserService;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.miftahari.distributor.libraries.myfunction;
import com.miftahari.distributor.libraries.response;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// import java.util.Optional;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/user")
public class DC001_UserController {

    @Autowired
    private D1001_UserService service;

    protected myfunction fun;
    protected response res;

    public DC001_UserController() {
        this.fun = new myfunction();
        this.res = new response();
    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(HttpServletRequest request, HttpServletResponse response, @RequestParam String user, @RequestParam String pass) throws Exception {
        return this.service.login(request, response, user, pass);
    }

    @GetMapping("/logout")
    public ResponseEntity<Object> logout(HttpServletRequest request, HttpServletResponse response) {
        return this.service.logout(request, response);
    }

    @GetMapping("/login/readsession/{key}")
    public String readSession(
        HttpServletRequest request, 
        HttpServletResponse response,
        @PathVariable String key) throws Exception {
        // return this.service.login(user, pass);
        
        Optional<String> cookieName = Arrays.stream(request.getCookies())
          .filter(c -> key.equals(c.getName()))
          .map(Cookie::getValue)
          .findAny();

        String value = cookieName.orElse("cookie_not_found");
        return this.fun.decrypt(value);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable long id) throws Exception {
        return this.service.get(id);
    }

    @GetMapping("/all")
    public ResponseEntity<Object> getAll() throws Exception {
        return this.service.getAll();
    }

    @PostMapping("/save")
    public ResponseEntity<Object> create(@RequestBody D1001_UserModel user) throws Exception {
        return this.service.save(user);
    }

    @PostMapping("/confirm/otp/")
    public ResponseEntity<Object> confirmOTP(@RequestParam String user, @RequestParam long kode) throws Exception {
        return this.service.confirmOTP(user, kode);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> update(
        @PathVariable long id, 
        @RequestParam String tlp, 
        @RequestParam String password, 
        @RequestParam String fullname, 
        @RequestParam String nickname, 
        @RequestParam String jk, 
        @RequestParam String alamat, 
        @RequestParam String foto) throws Exception {
        return this.service.update(id, tlp, password, fullname, nickname, jk, alamat,foto);
    }

    @PutMapping("/softdelete/{id}")
    public ResponseEntity<Object> softDelete(@PathVariable int id) throws Exception {
        return this.service.softDelete(id);
    }

    @DeleteMapping("/harddelete/{id}")
    public ResponseEntity<Object> hardDelete(@PathVariable int id) throws Exception {
        return this.service.hardDelete(id);
    }

}
