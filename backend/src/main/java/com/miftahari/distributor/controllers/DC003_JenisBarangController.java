package com.miftahari.distributor.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miftahari.distributor.models.D2002_JenisBarangModel;
import com.miftahari.distributor.services.D2002_JenisBarangService;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/barang/jenis/")
public class DC003_JenisBarangController {
    
    @Autowired
    private D2002_JenisBarangService service;

    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable long id) throws Exception {
        return this.service.get(id);
    }

    @GetMapping("/all")
    public ResponseEntity<Object> getAll() throws Exception {
        return this.service.getAll();
    }

    @PostMapping("/save")
    public ResponseEntity<Object> create(@RequestParam String jenis_barang) throws Exception {
        return this.service.save(jenis_barang);
    }
    
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> update(@PathVariable long id, @RequestParam String jenis_barang) throws Exception {
        return this.service.update(id, jenis_barang);
    }
    
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable long id) throws Exception {
        return this.service.delete(id);
    }

}
