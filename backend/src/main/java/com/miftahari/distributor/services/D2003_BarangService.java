package com.miftahari.distributor.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;

import com.miftahari.distributor.models.D2001_MerkBarangModel;
import com.miftahari.distributor.models.D2002_JenisBarangModel;
import com.miftahari.distributor.models.D2003_BarangModel;

import java.util.HashMap;
// import com.miftahari.distributor.repositories.D1001_UserRepository;
import java.util.List;
import java.util.Map;

// import java.util.Optional;
import com.miftahari.distributor.services.getIDService;
import com.miftahari.distributor.libraries.myfunction;
import com.miftahari.distributor.libraries.response;

@Service
public class D2003_BarangService {
    
    @Autowired
    private MongoTemplate mongoTemplate;

    protected getIDService ids;

    protected myfunction fun;
    protected response res;

    public D2003_BarangService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.ids = new getIDService(mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    } 

    public D2003_BarangService() {
        this.ids = new getIDService(this.mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    }

    @Cacheable(value = "barangCache", key = "#id")
    public ResponseEntity<Object> get(long id_barang) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query qd2003 = new Query(Criteria.where("id_barang").is(id_barang));
            if(mongoTemplate.exists(qd2003, D2003_BarangModel.class)) {
                List<D2003_BarangModel> d2003 = this.mongoTemplate.find(qd2003, D2003_BarangModel.class);
                
                Query qd2001 = new Query(Criteria.where("id_merkbarang").is(d2003.get(0).getMerk_barang()));
                List<D2001_MerkBarangModel> d2001 = this.mongoTemplate.find(qd2001, D2001_MerkBarangModel.class);

                Query qd2002 = new Query(Criteria.where("id_jenisbarang").is(d2003.get(0).getJenis_barang()));
                List<D2002_JenisBarangModel> d2002 = this.mongoTemplate.find(qd2002, D2002_JenisBarangModel.class);

                Map<String, Object> resmodel = new HashMap<String, Object>();

                resmodel.put("id_barang", d2003.get(0).getId_barang());
                resmodel.put("nama_barang", d2003.get(0).getNama_barang());
                resmodel.put("merk_barang", d2001);
                resmodel.put("jenis_barang", d2002);
                resmodel.put("satuan", d2003.get(0).getSatuan());
                resmodel.put("harga_beli", d2003.get(0).getHarga_beli());
                resmodel.put("harga_jual", d2003.get(0).getHarga_jual());
                resmodel.put("stok", d2003.get(0).getStok());
            
                finares.put("data", resmodel);
                finares.put("message", "success");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "barang not found!");
                return this.res.generateResponse(finares, 404);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get detail barang data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @Cacheable(value = "barangCache", key = "#id")
    public ResponseEntity<Object> getAll() {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("data", this.mongoTemplate.findAll(D2003_BarangModel.class));
            finares.put("message", "success");
            return this.res.generateResponse(finares, 200);
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get all barang data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "barangCache", allEntries = true)
    public ResponseEntity<Object> save(D2003_BarangModel model) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query1 = new Query(Criteria.where("nama_barang").is(model.getNama_barang().toString()));
        
            if(this.mongoTemplate.exists(query1, D2003_BarangModel.class)) {
                finares.put("message", "barang already exist!");
                return this.res.generateResponse(finares, 200);
            }
            else {
                model.setId_barang(this.ids.getId_barang(this.mongoTemplate));
                mongoTemplate.save(model);

                finares.put("data", model);
                finares.put("message", "success save barang");
                return this.res.generateResponse(finares, 201);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to create barang!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "barangCache", allEntries = true)
    public ResponseEntity<Object> update(long id_barang, D2003_BarangModel model) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_barang").is(id_barang));
            if(this.mongoTemplate.exists(query, D2003_BarangModel.class)) {
                Update update = new Update().set("nama_barang", model.getNama_barang().toString())
                                            .set("jenis_barang", model.getJenis_barang())
                                            .set("merk_barang", model.getMerk_barang())
                                            .set("satuan", model.getSatuan().toString())
                                            .set("harga_beli", model.getHarga_beli())
                                            .set("harga_jual", model.getHarga_jual())
                                            .set("stok", model.getStok());

                mongoTemplate.updateFirst(query, update, D2003_BarangModel.class);
                finares.put("data", update);
                finares.put("message", "success update barang");
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "barang not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to update barang!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "barangCache", allEntries = true)
    public ResponseEntity<Object> delete(long id_barang) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_barang").is(id_barang));
            if(this.mongoTemplate.exists(query, D2003_BarangModel.class)) {
                List<D2003_BarangModel> d2003 = this.mongoTemplate.find(query, D2003_BarangModel.class);
                this.mongoTemplate.remove(query , D2003_BarangModel.class);

                finares.put("data", d2003);
                finares.put("message", "success delete barang!");
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "barang not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to delete barang!");
            return this.res.generateResponse(finares, 500);
        }
    }

}
