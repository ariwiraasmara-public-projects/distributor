package com.miftahari.distributor.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.miftahari.distributor.models.D1005_PersonalAccessTokensModel;

import java.util.HashMap;
import java.util.Map;
import java.util.Base64;  

public class TokenService {
    
    public String abilities(int roles) {
        switch(roles) {
            case 1: // SUPER ADMIN
                return "*";
            case 2: // ADMIN SISTEM
                return "2C2R@d0001_sistemotp, " + 
                       "CR@d1001_user, " + 
                       "CR@d1002_userprofile, " + 
                       "CR@d1003_userwallet, " + 
                       "R@d1004_userpointhistory, " + 
                       "R@d1005_personalaccesstokens, " + 
                       "*@d2001_merkbarang, " + 
                       "*@d2002_jenisbarang, " + 
                       "*@d2003_barang, " + 
                       "*@d3001_distributor, " + 
                       "*@d3002_distributorprofile, " + 
                       "*@d3003_distributorstaff, " + 
                       "*@d3004_distributorstaffposition, " + 
                       "*@d4001_transaksi, " + 
                       "*@d4002_invoice, " + 
                       "*@d4003_receiveorder"; 
            case 3: // ADMIN GUDANG
                return "*@d2001_merkbarang, " + 
                       "*@d2002_jenisbarang, " + 
                       "*@d2003_barang, " + 
                       "*@d4001_transaksi, " + 
                       "*@d4002_invoice, " + 
                       "*@d4003_receiveorder"; 
            case 4: // ADMIN KAWASAN
                return "*@d0001_sistemotp, " +
                       "R@d2001_merkbarang, " + 
                       "R@d2002_jenisbarang, " + 
                       "R@d2003_barang, " + 
                       "R@d4001_transaksi, " + 
                       "R@d4002_invoice, " + 
                       "RU@d4003_receiveorder"; 
            case 5: // DISTRIBUTOR
                return "*@d1001_user, " + 
                       "R@d2001_merkbarang, " + 
                       "R@d2002_jenisbarang, " + 
                       "R@d2003_barang, " + 
                       "*@d3001_distributor, " + 
                       "*@d3002_distributorprofile, " + 
                       "*@d3003_distributorstaff, " + 
                       "*@d3004_distributorstaffposition, " + 
                       "*@d4001_transaksi, " + 
                       "R@d4002_invoice, " + 
                       "R@d4003_receiveorder";
            case 6: // STAFF ADMIN
                return "R@d1001_user, " + 
                       "R@d1002_userprofile, " + 
                       "R@d1003_userwallet, " + 
                       "R@d1004_userpointhistory, " + 
                       "R@d1005_personalaccesstokens, " + 
                       "R@d2001_merkbarang, " + 
                       "R@d2002_jenisbarang, " + 
                       "R@d2003_barang, " + 
                       "*@d4001_transaksi, " + 
                       "R@d4002_invoice, " + 
                       "R@d4003_receiveorder";
            case 7: // STAFF SALES, DLL
                return "R@d2001_merkbarang, " + 
                       "R@d2002_jenisbarang, " + 
                       "R@d2003_barang, " + 
                       "RU@d4001_transaksi, " + 
                       "R@d4002_invoice, " + 
                       "R@d4003_receiveorder";
            default: // NOTHING
                return "x";
        }
    }

    public String encode(String val) {
        try {
            Base64.Encoder encoder = Base64.getEncoder();
            return encoder.encodeToString(val.getBytes()).toString();
        }
        catch(Exception e) {
            e.printStackTrace();
            return "__null__";
        }
    }

    public String decode(String val) {
        try {
            Base64.Decoder decoder = Base64.getDecoder();  
            return new String(decoder.decode(val));  
        }
        catch(Exception e) {
            e.printStackTrace();
            return "__null__";
        }
    }

}
