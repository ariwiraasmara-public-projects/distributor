package com.miftahari.distributor.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;

import com.miftahari.distributor.models.D2003_BarangModel;
import com.miftahari.distributor.models.D4001_TransaksiModel;
import com.miftahari.distributor.models.D4002_InvoiceModel;
import com.miftahari.distributor.models.D4003_ReceiveOrderModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.miftahari.distributor.libraries.myfunction;
import com.miftahari.distributor.libraries.response;

@Service
public class D4003_ReceiveOrderService {
    
    @Autowired
    private MongoTemplate mongoTemplate;

    protected myfunction fun;
    protected response res;

    public D4003_ReceiveOrderService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.fun = new myfunction();
        this.res = new response();
    } 

    public D4003_ReceiveOrderService() {
        this.fun = new myfunction();
        this.res = new response();
    }

    @Cacheable(value = "receiveorderCache", key = "#id")
    public ResponseEntity<Object> get(String nomor_transaksi) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("nomor_transaksi").is(nomor_transaksi));
            if(mongoTemplate.exists(query, D4003_ReceiveOrderModel.class)) {
                List<D4003_ReceiveOrderModel> d4003 = this.mongoTemplate.find(query, D4003_ReceiveOrderModel.class);

                for(int x = 0; x < d4003.size(); x++) {
                    Query query2 = new Query(Criteria.where("id_barang").is(d4003.get(x).getBarang()));
                    List<D2003_BarangModel> d2003 = this.mongoTemplate.find(query2, D2003_BarangModel.class);
                    d4003.get(x).setBarang_detail(d2003);

                    Query query3 = new Query(Criteria.where("id_invoice").is(d4003.get(x).getInvoice()));
                    List<D4002_InvoiceModel> d4002 = this.mongoTemplate.find(query3, D4002_InvoiceModel.class);
                    d4003.get(x).setInvoice_detail(d4002);
                }

                finares.put("data", d4003);
                finares.put("message", "get detail receive order");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "receive order not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get detail receive order!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @Cacheable(value = "receiveorderCache", key = "#id")
    public ResponseEntity<Object> getAll(String id_distributor) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query1 = new Query(Criteria.where("distributor").is(id_distributor));
            if(mongoTemplate.exists(query1, D4001_TransaksiModel.class)) {
                List<D4003_ReceiveOrderModel> d4003 = this.mongoTemplate.find(query1, D4003_ReceiveOrderModel.class);

                for(int x = 0; x < d4003.size(); x++) {
                    Query query2 = new Query(Criteria.where("id_barang").is(d4003.get(x).getBarang()));
                    List<D2003_BarangModel> d2003 = this.mongoTemplate.find(query2, D2003_BarangModel.class);
                    d4003.get(x).setBarang_detail(d2003);
                
                    Query query3 = new Query(Criteria.where("id_invoice").is(d4003.get(x).getInvoice()));
                    List<D4002_InvoiceModel> d4002 = this.mongoTemplate.find(query3, D4002_InvoiceModel.class);
                    d4003.get(x).setInvoice_detail(d4002);
                }

                finares.put("data", d4003);
                finares.put("message", "get all receive order");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "receive order not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get all receive order!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @Cacheable(value = "receiveorderCache", key = "#id")
    public ResponseEntity<Object> update(String id_receiveorder, String nomor_transaksi) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("nomor_transaksi").is(nomor_transaksi));
            if(mongoTemplate.exists(query, D4001_TransaksiModel.class)) {
                List<D4003_ReceiveOrderModel> d4003 = this.mongoTemplate.find(query, D4003_ReceiveOrderModel.class);

                String tgl_sampai = this.fun.getDate().toString();
                String[] id_receiveorderArray = id_receiveorder.split(",");
                for(int x = 0; x < id_receiveorderArray.length; x++) {
                    Query query2 = new Query(Criteria.where("id_receiveorder").is(id_receiveorderArray[x]));

                    Update update = new Update().set("tgl_sampai", tgl_sampai);
                    mongoTemplate.updateFirst(query2, update, D4003_ReceiveOrderModel.class);

                    Update update2 = new Update().set("is_barang_exist", 1);
                    mongoTemplate.updateFirst(query2, update2, D4003_ReceiveOrderModel.class);

                    Query query3 = new Query(Criteria.where("id_barang").is(d4003.get(x).getBarang()));
                    List<D2003_BarangModel> d2003 = this.mongoTemplate.find(query3, D2003_BarangModel.class);
                    d4003.get(x).setBarang_detail(d2003);
                }

                Query query2 = new Query(Criteria.where("id_invoice").is(d4003.get(0).getInvoice()));
                List<D4002_InvoiceModel> d4002 = this.mongoTemplate.find(query2, D4002_InvoiceModel.class);

                Map<String, Object> resmodel = new HashMap<String, Object>();
                resmodel.put("receive_order", d4003);
                resmodel.put("invoice", d4002);

                finares.put("id_receiveorderArray", id_receiveorderArray);
                finares.put("data", resmodel);
                finares.put("message", "success update receive order");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "receive order not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to update receive order!");
            return this.res.generateResponse(finares, 500);
        }
    }
}
