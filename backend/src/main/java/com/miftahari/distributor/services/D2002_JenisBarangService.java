package com.miftahari.distributor.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;

import com.miftahari.distributor.models.D2002_JenisBarangModel;

import java.util.HashMap;
// import com.miftahari.distributor.repositories.D1001_UserRepository;
import java.util.List;
import java.util.Map;

// import java.util.Optional;
import com.miftahari.distributor.services.getIDService;
import com.miftahari.distributor.libraries.myfunction;
import com.miftahari.distributor.libraries.response;

@Service
public class D2002_JenisBarangService {
 
    @Autowired
    private MongoTemplate mongoTemplate;

    protected getIDService ids;

    protected myfunction fun;
    protected response res;

    public D2002_JenisBarangService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.ids = new getIDService(mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    } 

    public D2002_JenisBarangService() {
        this.ids = new getIDService(this.mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    }

    @Cacheable(value = "jenisbarangCache", key = "#id")
    public ResponseEntity<Object> get(long id_jenisbarang) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_jenisbarang").is(id_jenisbarang));
            if(mongoTemplate.exists(query, D2002_JenisBarangModel.class)) {
                List<D2002_JenisBarangModel> model = this.mongoTemplate.find(query, D2002_JenisBarangModel.class);

                finares.put("data", model);
                finares.put("message", "success");
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "jenis barang not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get detail jenis barang data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @Cacheable(value = "jenisbarangCache", key = "#id")
    public ResponseEntity<Object> getAll() {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("data", this.mongoTemplate.findAll(D2002_JenisBarangModel.class));
            finares.put("message", "success");
            return this.res.generateResponse(finares, 200);
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get all jenis barang data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "jenisbarangCache", allEntries = true)
    public ResponseEntity<Object> save(String jenis_barang) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query1 = new Query(Criteria.where("jenis_barang").is(jenis_barang));
            if(this.mongoTemplate.exists(query1, D2002_JenisBarangModel.class)) {
                finares.put("message", "jenis barang already exist!");
                return this.res.generateResponse(finares, 200);
            }
            else {
                D2002_JenisBarangModel model = new D2002_JenisBarangModel(
                    this.ids.getId_jenisbarang(this.mongoTemplate), 
                    jenis_barang
                );
                mongoTemplate.save(model);

                finares.put("data", model);
                finares.put("message", "success save jenis barang");
                return this.res.generateResponse(finares, 201);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to create jenis barang!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "jenisbarangCache", allEntries = true)
    public ResponseEntity<Object> update(long id_jenisbarang, String jenis_barang) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_jenisbarang").is(id_jenisbarang));
            if(this.mongoTemplate.exists(query, D2002_JenisBarangModel.class)) {
                Update update = new Update().set("jenis_barang", jenis_barang);

                mongoTemplate.updateFirst(query, update, D2002_JenisBarangModel.class);
                finares.put("data", update);
                finares.put("message", "success update jenis barang");
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "jenis barang not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to update jenis barang!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "jenisbarangCache", allEntries = true)
    public ResponseEntity<Object> delete(long id_jenisbarang) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_jenisbarang").is(id_jenisbarang));
            if(this.mongoTemplate.exists(query, D2002_JenisBarangModel.class)) {
                List<D2002_JenisBarangModel> model = this.mongoTemplate.find(query, D2002_JenisBarangModel.class);
                this.mongoTemplate.remove(query, D2002_JenisBarangModel.class);

                finares.put("data", model);
                finares.put("message", "success delete jenis barang!");
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "jenis barang not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to delete jenis barang!");
            return this.res.generateResponse(finares, 500);
        }
    }

}
