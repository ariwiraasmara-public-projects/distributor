package com.miftahari.distributor.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;

import com.miftahari.distributor.models.D1002_UserProfileModel;
import com.miftahari.distributor.models.D2003_BarangModel;
import com.miftahari.distributor.models.D3001_DistributorModel;
import com.miftahari.distributor.models.D4001_TransaksiModel;
import com.miftahari.distributor.models.D4002_InvoiceModel;
import com.miftahari.distributor.models.D4003_ReceiveOrderModel;
import com.miftahari.distributor.services.getIDService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.miftahari.distributor.libraries.myfunction;
import com.miftahari.distributor.libraries.response;

@Service
public class D4001_TransaksiService {
    
    @Autowired
    private MongoTemplate mongoTemplate;

    protected getIDService ids;

    protected myfunction fun;
    protected response res;

    public D4001_TransaksiService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.ids = new getIDService(mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    } 

    public D4001_TransaksiService() {
        this.ids = new getIDService(this.mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    }

    @Cacheable(value = "transaksiCache", key = "#id")
    public ResponseEntity<Object> get(String nomor_transaksi) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query1 = new Query(Criteria.where("nomor_transaksi").is(nomor_transaksi));
            if(mongoTemplate.exists(query1, D4001_TransaksiModel.class)) {
                List<D4001_TransaksiModel> d4001 = this.mongoTemplate.find(query1, D4001_TransaksiModel.class);
                List<D4003_ReceiveOrderModel> d4003 = this.mongoTemplate.find(query1, D4003_ReceiveOrderModel.class);
                
                Query query2 = new Query(Criteria.where("id_invoice").is(d4001.get(1)));
                List<D4002_InvoiceModel> d4002 = this.mongoTemplate.find(query2, D4002_InvoiceModel.class);
            
                Map<String, Object> resmodel = new HashMap<String, Object>();
                resmodel.put("transaction", d4001);
                resmodel.put("invoice", d4002);
                resmodel.put("receive_order", d4003);

                finares.put("data", resmodel);
                finares.put("message", "get trasaction detail!");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "transaction not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get transaction data!");
            return this.res.generateResponse(finares, 500);
        }
    }
    
    @Cacheable(value = "transaksiCache", key = "#id")
    public ResponseEntity<Object> getAll(String id_distributor) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("distributor").is(id_distributor));
            
            if(mongoTemplate.exists(query, D4001_TransaksiModel.class)) {
                List<D4001_TransaksiModel> model = this.mongoTemplate.find(query, D4001_TransaksiModel.class);

                finares.put("data", model);
                finares.put("message", "success");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "transaction not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get all transaction data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "transaksiCache", allEntries = true)
    public ResponseEntity<Object> toBasket(D4001_TransaksiModel model) {
        try {
            String id_transaksi = this.ids.getId_transaksi(mongoTemplate);
            model.setId_transaksi(id_transaksi);
            model.setStatus(3);
            mongoTemplate.save(model);

            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("data", model);
            finares.put("message", "success save items to basket!");
            finares.put("success", 1);
            return this.res.generateResponse(finares, 201);
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to save item data to basket!");
            return this.res.generateResponse(finares, 500);
        }
    }
        
    @CacheEvict(value = "transaksiCache", allEntries = true)
    public ResponseEntity<Object> setTransaction(String distributor, long id_user, String id_transaksi) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query1 = new Query(Criteria.where("distributor").is(distributor));
            if(mongoTemplate.exists(query1, D4001_TransaksiModel.class)) {
                String id_invoice = this.ids.getId_invoice().toString();
                String nomor_transaksi = this.ids.getNomor_transaksi().toString();
                String tgl_invoice = this.fun.getDate().toString();

                String[] id_transaksiArray = null;
                id_transaksiArray = id_transaksi.split(",");  

                for(int x = 0; x < id_transaksiArray.length; x++) {
                    Query query2 = new Query(Criteria.where("id_transaksi").is(id_transaksiArray[x]));

                    Update update = new Update().set("id_user", id_user)
                                            .set("invoice", id_invoice)
                                            .set("nomor_transaksi", nomor_transaksi)
                                            .set("status", 2);

                    mongoTemplate.updateFirst(query2, update, D4001_TransaksiModel.class);
                }

                D4002_InvoiceModel d4002 = new D4002_InvoiceModel(id_invoice, tgl_invoice, null, null);
                mongoTemplate.save(d4002);

                List<D4001_TransaksiModel> d4001 = this.mongoTemplate.find(query1, D4001_TransaksiModel.class);
                int x = 0;
                for(D4001_TransaksiModel id4001 : d4001) {
                    D4003_ReceiveOrderModel id4003 = new D4003_ReceiveOrderModel(
                        this.ids.getId_receiveorder().toString(),
                        id_invoice,
                        nomor_transaksi,
                        distributor,
                        0,
                        id4001.getBarang(),
                        id4001.getJumlah_barang(),
                        null, 
                        2
                    );
                    mongoTemplate.save(id4003);

                    Query query3 = new Query(Criteria.where("id_barang").is(id4001.getBarang()));
                    List<D2003_BarangModel> d2003 = this.mongoTemplate.find(query3, D2003_BarangModel.class);
                    d4001.get(x).setBarang_detail(d2003);

                    Query query4 = new Query(Criteria.where("id_distributor").is(distributor));
                    List<D3001_DistributorModel> d3001 = this.mongoTemplate.find(query4, D3001_DistributorModel.class);
                    d4001.get(x).setDistributor_detail(d3001);

                    Query query5 = new Query(Criteria.where("id_user").is(id4001.getUser()));
                    List<D1002_UserProfileModel> d1002 = this.mongoTemplate.find(query5, D1002_UserProfileModel.class);
                    d4001.get(x).setUser_detail(d1002);

                    x++;
                }

                Query query2 = new Query(Criteria.where("nomor_transaksi").is(nomor_transaksi));
                List<D4003_ReceiveOrderModel> d4003 = this.mongoTemplate.find(query2, D4003_ReceiveOrderModel.class);
                
                Map<String, Object> resmodel = new HashMap<String, Object>();
                resmodel.put("transaksi", d4001);
                resmodel.put("invoice", d4002);
                resmodel.put("receive_order", d4003);

                finares.put("data", resmodel);
                finares.put("message", "success save transaction!");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 201);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "transaction not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to save transaction data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "transaksiCache", allEntries = true)
    public ResponseEntity<Object> update(String nomor, String tujuan, String tgl_kirim) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("invoice").is(nomor));
            if(mongoTemplate.exists(query, D4001_TransaksiModel.class)) {
                Update ud4001 = new Update().set("status", 1)
                                            .set("updated_at", this.fun.getDate().toString());
                mongoTemplate.updateFirst(query, ud4001, D4001_TransaksiModel.class);

                
                Update ud4002 = new Update().set("tujuan", tujuan)
                                            .set("tgl_kirim", tgl_kirim);
                mongoTemplate.updateFirst(query, ud4002, D4002_InvoiceModel.class);
                
                List<D4001_TransaksiModel> d4001 = this.mongoTemplate.find(query, D4001_TransaksiModel.class);
                for(int x = 0; x < d4001.size(); x++) {
                    Query query2 = new Query(Criteria.where("id_barang").is(d4001.get(x).getBarang()));
                    List<D2003_BarangModel> d2003 = this.mongoTemplate.find(query2, D2003_BarangModel.class);
                    d4001.get(x).setBarang_detail(d2003);

                    Query query3 = new Query(Criteria.where("id_invoice").is(nomor));
                    List<D4002_InvoiceModel> d4002 = this.mongoTemplate.find(query3, D4002_InvoiceModel.class);
                    d4001.get(x).setInvoice_detail(d4002);

                    Query query4 = new Query(Criteria.where("id_distributor").is(d4001.get(x).getDistributor()));
                    List<D3001_DistributorModel> d3001 = this.mongoTemplate.find(query4, D3001_DistributorModel.class);
                    d4001.get(x).setDistributor_detail(d3001);

                    Query query5 = new Query(Criteria.where("id_user").is(d4001.get(x).getUser()));
                    List<D1002_UserProfileModel> d1002 = this.mongoTemplate.find(query5, D1002_UserProfileModel.class);
                    d4001.get(x).setUser_detail(d1002);
                }

                finares.put("data", d4001);
                finares.put("message", "update transaction success");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "transaction not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to update transaction data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    /* 
    @CacheEvict(value = "transaksiCache", allEntries = true)
    public ResponseEntity<Object> deleteOne(String id_transaksi) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_transaksi").is(id_transaksi));
            if(this.mongoTemplate.exists(query, D4001_TransaksiModel.class)) {
                List<D4001_TransaksiModel> model = this.mongoTemplate.find(query, D4001_TransaksiModel.class);
                this.mongoTemplate.remove(query , D4001_TransaksiModel.class);

                finares.put("data", model);
                finares.put("message", "success delete transaction!");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "transaction not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to delete transaction!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "transaksiCache", allEntries = true)
    public ResponseEntity<Object> delete(String nomor_transaksi) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("nomor_transaksi").is(nomor_transaksi));

            if(this.mongoTemplate.exists(query, D4001_TransaksiModel.class)) {
                List<D4001_TransaksiModel> model = this.mongoTemplate.find(query, D4001_TransaksiModel.class);
                this.mongoTemplate.remove(query , D4001_TransaksiModel.class);

                finares.put("data", model);
                finares.put("message", "success delete transaction!");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "transaction not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to delete transaction!");
            return this.res.generateResponse(finares, 500);
        }
    }
    */

}
