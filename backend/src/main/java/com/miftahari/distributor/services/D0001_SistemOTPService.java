package com.miftahari.distributor.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.miftahari.distributor.services.getIDService;
import com.miftahari.distributor.libraries.myfunction;
import com.miftahari.distributor.libraries.response;

public class D0001_SistemOTPService {
    
    @Autowired
    private MongoTemplate mongoTemplate;

    protected myfunction fun;
    protected response res;
    protected getIDService ids;

    public D0001_SistemOTPService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.ids = new getIDService(mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    }

    public D0001_SistemOTPService() {
        this.ids = new getIDService(this.mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    }

    public String create() {
        try {
            return String.valueOf(this.fun.randomNumber(6));
        }
        catch(Exception e) {
            return "isnull";
        }
    }

}
