package com.miftahari.distributor.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;

import com.miftahari.distributor.models.D0001_SistemOTPModel;
import com.miftahari.distributor.models.D1001_UserModel;
import com.miftahari.distributor.models.D1002_UserProfileModel;
import com.miftahari.distributor.models.D2003_BarangModel;
import com.miftahari.distributor.models.D3001_DistributorModel;
import com.miftahari.distributor.models.D3002_DistributorProfileModel;
import com.miftahari.distributor.models.D3003_DistributorStaffModel;
import com.miftahari.distributor.services.getIDService;
import com.miftahari.distributor.services.D1001_UserService;

import java.util.HashMap;
// import com.miftahari.distributor.repositories.D1001_UserRepository;
import java.util.List;
import java.util.Map;

import com.miftahari.distributor.libraries.myfunction;
import com.miftahari.distributor.libraries.response;

@Service
public class D3001_DistributorService {
    
    @Autowired
    private MongoTemplate mongoTemplate;

    protected getIDService ids;

    protected myfunction fun;
    protected response res;

    public D3001_DistributorService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.ids = new getIDService(mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    } 

    public D3001_DistributorService() {
        this.ids = new getIDService(this.mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    }

    @Cacheable(value = "distributorCache", key = "#id")
    public ResponseEntity<Object> get(String id_distributor) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_distributor").is(id_distributor));
            if(mongoTemplate.exists(query, D3001_DistributorModel.class)) {
                List<D3001_DistributorModel> d3001 = this.mongoTemplate.find(query, D3001_DistributorModel.class);
                if(d3001.get(0).getDeleted_at().equals("")) {
                    List<D3002_DistributorProfileModel> d3002 = this.mongoTemplate.find(query, D3002_DistributorProfileModel.class);
                    
                    Map<String, Object> resmodel = new HashMap<String, Object>();
                    resmodel.put("id_distributor", d3001.get(0).getId_distributor());
                    resmodel.put("nama", d3001.get(0).getNama_distributor());
                    
                    resmodel.put("deskripsi", d3002.get(0).getDeskripsi());
                    resmodel.put("alamat", d3002.get(0).getAlamat());
                    resmodel.put("foto", d3002.get(0).getFoto());

                    resmodel.put("longitude", d3001.get(0).getLongitude());
                    resmodel.put("latitude", d3001.get(0).getLatitude());
                    resmodel.put("created_at", d3001.get(0).getCreated_at());
                    resmodel.put("updated_at", d3001.get(0).getUpdated_at());

                    finares.put("data", resmodel);
                    finares.put("message", "success");
                    finares.put("success", 1);
                    return this.res.generateResponse(finares, 200);
                }
                else {
                    finares.put("message", "this distributor has been deleted");
                    finares.put("error", 3);
                    return this.res.generateResponse(finares, 404);
                }
            }
            else {
                finares.put("error", 2);
                finares.put("message", "distributor not found!");
                return this.res.generateResponse(finares, 404);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get distributor data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @Cacheable(value = "distributorCache", key = "#id")
    public ResponseEntity<Object> getAllStaff(String id_distributor) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query1 = new Query(Criteria.where("id_distributor").is(id_distributor));
            if(mongoTemplate.exists(query1, D3001_DistributorModel.class)) {
                List<D3001_DistributorModel> d3001 = this.mongoTemplate.find(query1, D3001_DistributorModel.class);
                if(d3001.get(0).getDeleted_at().equals("")) {
                    List<D3002_DistributorProfileModel> d3002 = this.mongoTemplate.find(query1, D3002_DistributorProfileModel.class);
                    List<D3003_DistributorStaffModel> d3003 = this.mongoTemplate.find(query1, D3003_DistributorStaffModel.class);
                    
                    for(D3003_DistributorStaffModel id3003 : d3003) {
                        Query query2 = new Query(Criteria.where("id_user").is(id3003.getId_user()));
                        List<D1002_UserProfileModel> id1002 = this.mongoTemplate.find(query2, D1002_UserProfileModel.class);
                        d3003.get(0).setUser_detail(id1002);
                    }

                    Query query3 = new Query(Criteria.where("id_user").is(d3001.get(0).getUser()));
                    List<D1002_UserProfileModel> d1002 = this.mongoTemplate.find(query3, D1002_UserProfileModel.class);

                    Map<String, Object> resmodel = new HashMap<String, Object>();
                    resmodel.put("id_distributor", d3001.get(0).getId_distributor());
                    resmodel.put("nama_distributor", d3001.get(0).getNama_distributor());
                    
                    resmodel.put("deskripsi", d3002.get(0).getDeskripsi());
                    resmodel.put("alamat", d3002.get(0).getAlamat());
                    resmodel.put("foto", d3002.get(0).getFoto());

                    resmodel.put("longitude", d3001.get(0).getLongitude());
                    resmodel.put("latitude", d3001.get(0).getLatitude());
                    resmodel.put("created_at", d3001.get(0).getCreated_at());
                    resmodel.put("updated_at", d3001.get(0).getUpdated_at());

                    resmodel.put("user", d1002);
                    resmodel.put("distributor_staff", d3003);

                    finares.put("data", resmodel);
                    finares.put("message", "success");
                    finares.put("success", 1);
                    return this.res.generateResponse(finares, 404);
                }
                else {
                    finares.put("message", "this distributor has been deleted");
                    finares.put("error", 3);
                    return this.res.generateResponse(finares, 404);
                }
            }
            else {
                finares.put("error", 2);
                finares.put("message", "distributor not found!");
                return this.res.generateResponse(finares, 404);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get all distributor's staff data!");
            return this.res.generateResponse(finares, 500);
        }
    }
    
    @Cacheable(value = "distributorCache", key = "#id")
    public ResponseEntity<Object> getAll() {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            List<D3001_DistributorModel> ld3001 = this.mongoTemplate.findAll(D3001_DistributorModel.class);

            for(int x = 0; x < ld3001.size(); x++) {
                Query query = new Query();
                query.addCriteria(Criteria.where("id_user").is(ld3001.get(x).getUser()));
                List<D1002_UserProfileModel> lid1001 = this.mongoTemplate.find(query, D1002_UserProfileModel.class);
                ld3001.get(x).setUser_detail(lid1001);
            }

            // LookupOperation lookupOperation = LookupOperation.newLookup()
            //                                     .from("d1001_user")
            //                                     .localField("user")
            //                                     .foreignField("id_user")
            //                                     .as("user");
            
            // Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(Criteria.where("_id").exists(true)) , lookupOperation);
            // List<D3001_DistributorModel> ld3001 = mongoTemplate.aggregate(aggregation, "d3001_distributor", D3001_DistributorModel.class).getMappedResults();
            // LOGGER.info("Obj Size " +results.size());

            finares.put("data", ld3001);
            finares.put("message", "success");
            finares.put("success", 1);
            return this.res.generateResponse(finares, 200);
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get all distributor data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "distributorCache", allEntries = true)
    public ResponseEntity<Object> save(String nama_distributor, String username, String email, String tlp, String password) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query1 = new Query(Criteria.where("nama_distributor").is(nama_distributor));
            Query query2 = new Query(Criteria.where("username").is(username));
            Query query3 = new Query(Criteria.where("email").is(email));
            if(mongoTemplate.exists(query1, D3001_DistributorModel.class) || mongoTemplate.exists(query2, D1001_UserModel.class) || mongoTemplate.exists(query3, D1001_UserModel.class)) {
                finares.put("message", "distributor name or username or user mail already exist!");
                finares.put("error", 2);
                return this.res.generateResponse(finares, 400);
            }

            D1001_UserModel usermodel = new D1001_UserModel();
            long id_user = this.ids.getId_user(this.mongoTemplate);
            usermodel.setId_user(id_user);
            usermodel.setUsername(username.toString());
            usermodel.setEmail(email.toString());
            usermodel.setTlp(tlp.toString());
            usermodel.setPassword(password);
            usermodel.setRoles(5); // roles Distributor
            usermodel.setToken(fun.random(10));
            usermodel.setStatus(2);
            D1001_UserService user_service = new D1001_UserService(this.mongoTemplate);
            user_service.save(usermodel);

            String id_distributor = this.ids.getId_distributor(this.mongoTemplate);
            String date = fun.getDate();
            D3001_DistributorModel d3001 = new D3001_DistributorModel(
                id_distributor,
                id_user,
                nama_distributor.toString(),
                0,
                0,
                date,
                "",
                ""
            );
            mongoTemplate.save(d3001);

            D3002_DistributorProfileModel d3002 = new D3002_DistributorProfileModel(
                id_distributor,"","",""
            );
            mongoTemplate.save(d3002);

            D3003_DistributorStaffModel d3003 = new D3003_DistributorStaffModel(
                id_user, id_distributor, "Distributor", null
            );
            mongoTemplate.save(d3003);

            Map<String, Object> resmodel = new HashMap<String, Object>();
            resmodel.put("id_user", id_user);
            resmodel.put("id_distributor", id_distributor);
            resmodel.put("username", usermodel.getUsername().toString());
            resmodel.put("email", usermodel.getEmail().toString());
            resmodel.put("no_telpon", usermodel.getTlp().toString());
            resmodel.put("password_user", usermodel.getPassword().toString());
            resmodel.put("nama_distributor", d3001.getNama_distributor().toString());
            resmodel.put("created_at", date);

            finares.put("data", resmodel);
            finares.put("message", "save distributor success");
            finares.put("success", 1);
            return this.res.generateResponse(finares, 201);
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to save data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "distributorCache", allEntries = true)
    public ResponseEntity<Object> update(
        String id_distributor, 
        String nama_distributor,
        float longitude,
        float latitude,
        String deskripsi,
        String alamat,
        String foto) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_distributor").is(id_distributor));
            if(mongoTemplate.exists(query, D3001_DistributorModel.class)) {
                Update ud3001 = new Update().set("nama_distributor", nama_distributor)
                                            .set("longitude", longitude)
                                            .set("latitude", latitude);

                Update ud3002 = new Update().set("deskripsi", deskripsi)
                                            .set("alamat", alamat)
                                            .set("foto", foto);

                mongoTemplate.updateFirst(query, ud3001, D3001_DistributorModel.class);
                mongoTemplate.updateFirst(query, ud3002, D3002_DistributorProfileModel.class);

                Map<String, Object> resmodel = new HashMap<String, Object>();
                resmodel.put("distributor", ud3001);
                resmodel.put("distributor_profile", ud3002);

                finares.put("data", resmodel);
                finares.put("message", "update distributor success");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "distributor not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to update data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "distributorCache", allEntries = true)
    public ResponseEntity<Object> softDelete(String id_distributor) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query3001 = new Query(Criteria.where("id_distributor").is(id_distributor));
            if(mongoTemplate.exists(query3001, D3001_DistributorModel.class)) {
                Update ud3001 = new Update().set("deleted_at", this.fun.getDate());
                mongoTemplate.updateFirst(query3001, ud3001, D3001_DistributorModel.class);

                List<D3003_DistributorStaffModel> ld3003 = this.mongoTemplate.find(query3001, D3003_DistributorStaffModel.class);
                for(D3003_DistributorStaffModel d3003 : ld3003) {
                    Query query1001 = new Query(Criteria.where("id_user").is(d3003.getId_user()));
                    Update ud1001 = new Update().set("status", 0);
                    mongoTemplate.updateFirst(query1001, ud1001, D1001_UserModel.class);
                }

                finares.put("data", ud3001);
                finares.put("message", "soft delete success!");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "distributor not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to soft delete data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "distributorCache", allEntries = true)
    public ResponseEntity<Object> hardDelete(String id_distributor) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query3001 = new Query(Criteria.where("id_distributor").is(id_distributor));
            if(mongoTemplate.exists(query3001, D3001_DistributorModel.class)) {
                List<D3001_DistributorModel> ld3001 = this.mongoTemplate.find(query3001, D3001_DistributorModel.class);
                List<D3003_DistributorStaffModel> ld3003 = this.mongoTemplate.find(query3001, D3003_DistributorStaffModel.class);

                mongoTemplate.remove(query3001 , D3001_DistributorModel.class);
                mongoTemplate.remove(query3001 , D3002_DistributorProfileModel.class);
                mongoTemplate.remove(query3001 , D3003_DistributorStaffModel.class);

                for(D3003_DistributorStaffModel d3003 : ld3003 ) {
                    Query query1001 = new Query(Criteria.where("id_user").is(d3003.getId_user()));
                    mongoTemplate.remove(query1001 , D1001_UserModel.class);
                }

                finares.put("data", ld3001);
                finares.put("message", "hard deleted success!");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);  
            }
            else {
                finares.put("error", 2);
                finares.put("message", "distributor not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to hard delete data!");
            return this.res.generateResponse(finares, 500);
        }
    }
}
