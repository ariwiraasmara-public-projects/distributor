package com.miftahari.distributor.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;

import com.miftahari.distributor.models.D2003_BarangModel;
import com.miftahari.distributor.models.D3001_DistributorModel;
import com.miftahari.distributor.models.D4001_TransaksiModel;
import com.miftahari.distributor.models.D4002_InvoiceModel;
import com.miftahari.distributor.models.D4003_ReceiveOrderModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// import java.util.Optional;
import com.miftahari.distributor.services.getIDService;
import com.miftahari.distributor.libraries.myfunction;
import com.miftahari.distributor.libraries.response;

@Service
public class D4002_InvoiceService {
    @Autowired
    private MongoTemplate mongoTemplate;

    protected getIDService ids;

    protected myfunction fun;
    protected response res;

    public D4002_InvoiceService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.ids = new getIDService(mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    } 

    public D4002_InvoiceService() {
        this.ids = new getIDService(this.mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    }

    @Cacheable(value = "invoiceCache", key = "#id")
    public ResponseEntity<Object> get(String id_invoice) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("invoice").is(id_invoice));
            if(mongoTemplate.exists(query, D4001_TransaksiModel.class)) {
                List<D4001_TransaksiModel> d4001 = this.mongoTemplate.find(query, D4001_TransaksiModel.class);
                List<D4002_InvoiceModel> d4002 = this.mongoTemplate.find(query, D4002_InvoiceModel.class);
                List<D4003_ReceiveOrderModel> d4003 = this.mongoTemplate.find(query, D4003_ReceiveOrderModel.class);

                for(int x = 0; x < d4001.size(); x++) {
                    Query query2 = new Query(Criteria.where("id_barang").is(d4001.get(x).getBarang()));
                    List<D2003_BarangModel> d2003 = this.mongoTemplate.find(query2, D2003_BarangModel.class);
                    d4001.get(x).setBarang_detail(d2003);
                }

                Map<String, Object> resmodel = new HashMap<String, Object>();
                resmodel.put("invoice", d4002);
                resmodel.put("transaksi", d4001);
                resmodel.put("receive_order", d4003);

                finares.put("data", resmodel);
                finares.put("message", "success");
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "invoice not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get detail invoice data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @Cacheable(value = "invoiceCache", key = "#id")
    public ResponseEntity<Object> getAll(String id_distributor) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("distributor").is(id_distributor));
            if(mongoTemplate.exists(query, D4001_TransaksiModel.class)) {
                // List<D4001_TransaksiModel> d4001 = mongoTemplate.findDistinct(query, "invoice", D4001_TransaksiModel.class, D4001_TransaksiModel.class);
                List<D4001_TransaksiModel> d4001 = this.mongoTemplate.find(query, D4001_TransaksiModel.class);

                Query query2 = new Query(Criteria.where("id_distributor").is(id_distributor));
                List<D3001_DistributorModel> d3002 = this.mongoTemplate.find(query2, D3001_DistributorModel.class);
                for(int x = 0; x < d4001.size(); x++) {
                    Query query3 = new Query(Criteria.where("id_invoice").is(d4001.get(x).getInvoice()));
                    List<D4002_InvoiceModel> d4002 = this.mongoTemplate.find(query3, D4002_InvoiceModel.class);
                    d4001.get(x).setInvoice_detail(d4002);
                    d4001.get(x).setDistributor_detail(d3002);
                }

                finares.put("data", d4001);
                finares.put("message", "success");
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "invoice not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get all invoice data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "invoiceCache", allEntries = true)
    public ResponseEntity<Object> update(String id_invoice, String tujuan, String tgl_kirim) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_invoice").is(id_invoice));
            if(this.mongoTemplate.exists(query, D4002_InvoiceModel.class)) {
                Update update = new Update().set("tujuan", tujuan)
                                            .set("tgl_kirim", tgl_kirim);

                mongoTemplate.updateFirst(query, update, D4002_InvoiceModel.class);
                finares.put("data", update);
                finares.put("message", "success update invoice");
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "invoice not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to update invoice!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "invoiceCache", allEntries = true)
    public ResponseEntity<Object> delete(String id_invoice) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_invoice").is(id_invoice));
            if(this.mongoTemplate.exists(query, D4002_InvoiceModel.class)) {
                List<D4002_InvoiceModel> model = this.mongoTemplate.find(query, D4002_InvoiceModel.class);
                this.mongoTemplate.remove(query , D4002_InvoiceModel.class);

                Query query2 = new Query(Criteria.where("invoice").is(id_invoice));
                this.mongoTemplate.remove(query2 , D4001_TransaksiModel.class);
                this.mongoTemplate.remove(query2 , D4003_ReceiveOrderModel.class);

                finares.put("data", model);
                finares.put("message", "success delete invoice!");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "transaction not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to delete transaction!");
            return this.res.generateResponse(finares, 500);
        }
    }
}
