package com.miftahari.distributor.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import com.miftahari.distributor.models.D0001_SistemOTPModel;
import com.miftahari.distributor.models.D1001_UserModel;
import com.miftahari.distributor.models.D1005_PersonalAccessTokensModel;
import com.miftahari.distributor.models.D2001_MerkBarangModel;
import com.miftahari.distributor.models.D2002_JenisBarangModel;
import com.miftahari.distributor.models.D2003_BarangModel;
import com.miftahari.distributor.models.D3001_DistributorModel;
import com.miftahari.distributor.models.D4001_TransaksiModel;
import com.miftahari.distributor.libraries.myfunction;
import com.mongodb.client.MongoClient;

public class getIDService {
    
    @Autowired
    private MongoTemplate mongoTemplate;

    protected myfunction fun;

    public getIDService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.fun = new myfunction();
    }

    public Long getId_otp(MongoTemplate mongoTemplate) {
        try {
            Query query = new Query();
            query.with(Sort.by(Sort.Order.desc("id_otp"))).limit(1);
            if(mongoTemplate.exists(query, D0001_SistemOTPModel.class)) {
                List<D0001_SistemOTPModel> model = mongoTemplate.find(query, D0001_SistemOTPModel.class);
                long auto_increment = model.get(0).getId_otp() + 1;
                return auto_increment;
            }
            else {
                return Long.parseLong("1");
            }
        }
        catch(Exception e) {
            long nol = 0;
            e.printStackTrace();
            return nol;
        }
    }

    public Long getId_user(MongoTemplate mongoTemplate) {
        try {
            Query query = new Query();
            query.with(Sort.by(Sort.Order.desc("id_user"))).limit(1);
            if(mongoTemplate.exists(query, D1001_UserModel.class)) {
                List<D1001_UserModel> model = mongoTemplate.find(query, D1001_UserModel.class);
                long auto_increment = model.get(0).getId_user() + 1;
                return auto_increment;
            }
            else {
                return Long.parseLong("1");
            }
        }
        catch(Exception e) {
            long nol = 0;
            e.printStackTrace();
            return nol;
        }
    } 

    public Long getId_personalaccesstokens(MongoTemplate mongoTemplate) {
        try {
            Query query = new Query();
            query.with(Sort.by(Sort.Order.desc("id_user"))).limit(1);
            if(mongoTemplate.exists(query, D1005_PersonalAccessTokensModel.class)) {
                List<D1005_PersonalAccessTokensModel> model = mongoTemplate.find(query, D1005_PersonalAccessTokensModel.class);
                long auto_increment = model.get(0).getId_user() + 1;
                return auto_increment;
            }
            else {
                return Long.parseLong("1");
            }
        }
        catch(Exception e) {
            long nol = 0;
            e.printStackTrace();
            return nol;
        }
    }

    public Long getId_merkbarang(MongoTemplate mongoTemplate) {
        try {
            Query query = new Query();
            query.with(Sort.by(Sort.Order.desc("id_merkbarang"))).limit(1);
            if(mongoTemplate.exists(query, D2001_MerkBarangModel.class)) {
                List<D2001_MerkBarangModel> model = mongoTemplate.find(query, D2001_MerkBarangModel.class);
                long auto_increment = model.get(0).getId_merkbarang() + 1;
                return auto_increment;
            }
            else {
                return Long.parseLong("1");
            }
        }
        catch(Exception e) {
            long nol = 0;
            e.printStackTrace();
            return nol;
        }
    }

    public Long getId_jenisbarang(MongoTemplate mongoTemplate) {
        try {
            Query query = new Query();
            query.with(Sort.by(Sort.Order.desc("id_jenisbarang"))).limit(1);
            if(mongoTemplate.exists(query, D2002_JenisBarangModel.class)) {
                List<D2002_JenisBarangModel> model = mongoTemplate.find(query, D2002_JenisBarangModel.class);
                long auto_increment = model.get(0).getId_jenisbarang() + 1;
                return auto_increment;
            }
            else {
                return Long.parseLong("1");
            }
        }
        catch(Exception e) {
            long nol = 0;
            e.printStackTrace();
            return nol;
        }
    }

    public Long getId_barang(MongoTemplate mongoTemplate) {
        try {
            Query query = new Query();
            query.with(Sort.by(Sort.Order.desc("id_barang"))).limit(1);
            if(mongoTemplate.exists(query, D2003_BarangModel.class)) {
                List<D2003_BarangModel> model = mongoTemplate.find(query, D2003_BarangModel.class);
                long auto_increment = model.get(0).getId_barang() + 1;
                return auto_increment;
            }
            else {
                return Long.parseLong("1");
            }
        }
        catch(Exception e) {
            long nol = 0;
            e.printStackTrace();
            return nol;
        }
    }

    public String getId_distributor(MongoTemplate mongoTemplate) {
        try {
            // FORMAT IDD@0000000000000001
            Query query = new Query();
            query.with(Sort.by(Sort.Order.desc("id_distributor"))).limit(1);
            if(mongoTemplate.exists(query, D3001_DistributorModel.class)) {
                List<D3001_DistributorModel> model = mongoTemplate.find(query, D3001_DistributorModel.class);
                String current_id = model.get(0).getId_distributor();
                String numberPart = current_id.substring(current_id.lastIndexOf("@") + 1);
                long currentNumber = Long.parseLong(numberPart);
                long newNumber = currentNumber + 1;
                String newNumberString = String.format("%016d", newNumber);
                String result = current_id.replaceAll(numberPart, newNumberString);
                return result; 
            }
            else {
                return "IDD@0000000000000001";
            }
        }
        catch(Exception e) {
            return e.toString();
        }
    }

    public String getId_transaksi(MongoTemplate mongoTemplate) {
        try {
            // FORMAT IDTyyyyMMdd@0000000000000000001
            return "IDT" + this.fun.randomNumber(25) + this.fun.getDateString("yyyyMMddhhmmss") + this.fun.randomNumber(25);
            // Query query = new Query();
            // query.with(Sort.by(Sort.Order.desc("id_transaksi"))).limit(1);
            // if(mongoTemplate.exists(query, D4001_TransaksiModel.class)) {
            //     List<D4001_TransaksiModel> model = mongoTemplate.find(query, D4001_TransaksiModel.class);
            //     String current_id = model.get(0).getId_transaksi();
                
            //     int tIndex = current_id.indexOf('T');
            //     int atIndex = current_id.indexOf("@");
            //     String today = current_id.substring(tIndex, atIndex);

            //     if(today.equals(this.fun.getDateString("yyyyMMdd"))) {
            //         String numberPart = current_id.substring(current_id.lastIndexOf("@") + 1);
            //         long newNumber = Long.parseLong(numberPart) + 1;
            //         String newNumberString = String.format("%019d", newNumber);
            //         String result = current_id.replaceAll(numberPart, newNumberString);
            //         return result; 
            //     }
            //     else {
            //         return "IDT" + this.fun.getDateString("yyyyMMdd") + "@0000000000000000001";
            //     }
            // }
            // else {
            //     return "IDT" + this.fun.getDateString("yyyyMMdd") + "@0000000000000000001";
            //     // return "IDT@0000000000000000001";
            // }
        }
        catch(Exception e) {
            return e.toString();
        }
    }

    public String getNomor_transaksi() {
        try {
            return "TR-" + this.fun.getDateString("yyyyMMddhhmmss") + this.fun.random(5);
        }
        catch(Exception e) {
            return e.toString();
        }
    }

    public String getId_invoice() {
        try {
            return "INV" + this.fun.getDateString("yyyyMMddhhmmss") + this.fun.random(5);
        }
        catch(Exception e) {
            return e.toString();
        }
    }

    public String getId_receiveorder() {
        try {
            return "RO" + this.fun.getDateString("yyyyMMddhhmmss") + this.fun.random(3);
        }
        catch(Exception e) {
            return e.toString();
        }
    }

}
