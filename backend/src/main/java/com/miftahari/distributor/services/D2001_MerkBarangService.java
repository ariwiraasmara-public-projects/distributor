package com.miftahari.distributor.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.ResponseEntity;

import com.miftahari.distributor.models.D2001_MerkBarangModel;

import java.util.HashMap;
// import com.miftahari.distributor.repositories.D1001_UserRepository;
import java.util.List;
import java.util.Map;

// import java.util.Optional;
import com.miftahari.distributor.services.getIDService;
import com.miftahari.distributor.libraries.myfunction;
import com.miftahari.distributor.libraries.response;

@Service
public class D2001_MerkBarangService {
    
    @Autowired
    private MongoTemplate mongoTemplate;

    protected getIDService ids;

    protected myfunction fun;
    protected response res;

    public D2001_MerkBarangService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.ids = new getIDService(mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    } 

    public D2001_MerkBarangService() {
        this.ids = new getIDService(this.mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
    }

    @Cacheable(value = "merkbarangCache", key = "#id")
    public ResponseEntity<Object> get(long id_merkbarang) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_merkbarang").is(id_merkbarang));
            if(mongoTemplate.exists(query, D2001_MerkBarangModel.class)) {
                List<D2001_MerkBarangModel> model = this.mongoTemplate.find(query, D2001_MerkBarangModel.class);

                finares.put("data", model);
                finares.put("message", "success");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "merk barang not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get detail merk barang data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @Cacheable(value = "merkbarangCache", key = "#id")
    public ResponseEntity<Object> getAll() {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("data", this.mongoTemplate.findAll(D2001_MerkBarangModel.class));
            finares.put("message", "success");
            finares.put("success", 1);
            return this.res.generateResponse(finares, 200);
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to get all merk barang data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "merkbarangCache", allEntries = true)
    public ResponseEntity<Object> save(D2001_MerkBarangModel model) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query1 = new Query(Criteria.where("merk_barang").is(model.getMerk_barang().toString()));
            if(this.mongoTemplate.exists(query1, D2001_MerkBarangModel.class)) {
                finares.put("message", "merk barang already exist!");
                return this.res.generateResponse(finares, 200);
            }
            else {
                model.setId_merkbarang(this.ids.getId_merkbarang(this.mongoTemplate));
                mongoTemplate.save(model);

                finares.put("data", model);
                finares.put("message", "success save merk barang");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 201);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", "1");
            finares.put("message", "fail to create merk barang!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "merkbarangCache", allEntries = true)
    public ResponseEntity<Object> update(long id_merkbarang, D2001_MerkBarangModel model) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_merkbarang").is(id_merkbarang));
            if(this.mongoTemplate.exists(query, D2001_MerkBarangModel.class)) {
                Update update = new Update().set("merk_barang", model.getMerk_barang().toString())
                                            .set("deskripsi", model.getDeskripsi().toString());

                mongoTemplate.updateFirst(query, update, D2001_MerkBarangModel.class);
                finares.put("data", update);
                finares.put("message", "success update merk barang");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "merk barang not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to update merk barang!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "merkbarangCache", allEntries = true)
    public ResponseEntity<Object> delete(long id_merkbarang) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_merkbarang").is(id_merkbarang));
            if(this.mongoTemplate.exists(query, D2001_MerkBarangModel.class)) {
                List<D2001_MerkBarangModel> model = this.mongoTemplate.find(query, D2001_MerkBarangModel.class);
                this.mongoTemplate.remove(query , D2001_MerkBarangModel.class);

                finares.put("data", model);
                finares.put("message", "success delete merk barang!");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "merk barang not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to delete merk barang!");
            return this.res.generateResponse(finares, 500);
        }
    }

}
