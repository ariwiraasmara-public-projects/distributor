package com.miftahari.distributor.services;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.http.ResponseEntity;

import com.miftahari.distributor.models.D0001_SistemOTPModel;
import com.miftahari.distributor.models.D1001_UserModel;
import com.miftahari.distributor.models.D1002_UserProfileModel;
import com.miftahari.distributor.models.D1003_UserWalletModel;
import com.miftahari.distributor.models.D1004_UserPoinHistoryModel;
import com.miftahari.distributor.models.D1005_PersonalAccessTokensModel;
import com.miftahari.distributor.models.D3001_DistributorModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// import java.util.Optional;
import com.miftahari.distributor.services.getIDService;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.miftahari.distributor.services.TokenService;
import com.miftahari.distributor.libraries.myfunction;
import com.miftahari.distributor.libraries.response;
import com.miftahari.distributor.libraries.mycookie;

@Service
public class D1001_UserService {

    @Autowired
    private MongoTemplate mongoTemplate;

    protected Logger log;

    protected myfunction fun;
    protected response res;
    protected getIDService ids;
    protected TokenService token;
    protected mycookie cookie;

    public D1001_UserService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.ids = new getIDService(mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
        this.token = new TokenService();
        this.cookie = new mycookie();
    }

    public D1001_UserService() {
        this.ids = new getIDService(this.mongoTemplate);
        this.fun = new myfunction();
        this.res = new response();
        this.token = new TokenService();
        this.cookie = new mycookie();
    }

    public String userRole(int role) {
        try {
            // log.info("getting user role, value " + role);
            switch (role) {
                case 1:
                return "Super Admin";
                case 2:
                return "Admin System";
                case 3:
                    return "Admin Gudang";
                case 4:
                    return "Admin Kawasan";
                case 5:
                    return "Distributor";
                case 6:
                    return "Staff Admin";
                case 7:
                    return "Staff Sales";
                case 8:
                    return "Staff";
                default:
                return "none";
            }
        }
        catch(Exception e) {
            // log.error("user role error");
            e.printStackTrace();
            return "x";
        }
        
    }

    public String userStatus(int status) {
        try {
            // log.info("get user status, value " + status);
            switch (status) {
                case 1:
                return "active";
                case 2:
                return "pending";
                default:
                return "inactive";
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            return "x";
        }
        
    }

    public ResponseEntity<Object> login(HttpServletRequest request, HttpServletResponse response, String user, String password) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query1 = new Query(Criteria.where("username").is(user));
            Query query2 = new Query(Criteria.where("email").is(user));
            Query query3 = new Query(Criteria.where("password").is(password));

            if( (this.mongoTemplate.exists(query1, D1001_UserModel.class) || this.mongoTemplate.exists(query2, D1001_UserModel.class)) && this.mongoTemplate.exists(query3, D1001_UserModel.class) ) {
                List<D1001_UserModel> d1001 = this.mongoTemplate.find(query1, D1001_UserModel.class);
                
                Query query4 = new Query(Criteria.where("user").is(d1001.get(0).getId_user()));
                List<D3001_DistributorModel> d3001 = this.mongoTemplate.find(query4, D3001_DistributorModel.class);

                String[] cookieNames = {"--dx1--", "--dx2--", "--dx3--", "--dx4--"};
                String[] cookiePaths = {"/", "/", "/", "/"};
                String[] cookieValues = {
                    d1001.get(0).getUsername().toString(), 
                    d1001.get(0).getEmail().toString(), 
                    String.valueOf(d1001.get(0).getRoles()),
                    d3001.get(0).getId_distributor().toString()
                };

                this.cookie.setCookie(response, "islogin", "1", "/", false);
                this.cookie.setCookies(response, cookieNames, cookiePaths, cookieValues);

                Map<String, Object> resmodel = new HashMap<String, Object>();
                resmodel.put("islogin", 1);
                resmodel.put("--dx1--", d1001.get(0).getUsername());
                resmodel.put("--dx2--", d1001.get(0).getEmail());
                resmodel.put("--dx3--", d1001.get(0).getRoles());
                resmodel.put("--dx4--", d3001.get(0).getId_distributor());

                finares.put("data", resmodel);
                finares.put("message", "you're login!");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "fail to login!");
                return this.res.generateResponse(finares, 500);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to login!");
            return this.res.generateResponse(finares, 500);
        }
    }

    public ResponseEntity<Object> logout(HttpServletRequest request, HttpServletResponse response) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();

            this.cookie.deleteCookie(response, "islogin", "/");
            String[] cookieNames = {"islogin", "--dx1--", "--dx2--", "--dx3--", "--dx4--"};
            String[] cookiePaths = {"/", "/", "/", "/"};
            this.cookie.deleteCookies(response, cookieNames, cookiePaths);

            finares.put("message", "you're logout!");
            finares.put("success", 1);
            return this.res.generateResponse(finares, 200);
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to logout!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @Cacheable(value = "userCache", key = "#id")
    public ResponseEntity<Object> get(long id_user) {
        try {
            // log.info("get user detail by id : " + id_user);
            Query query = new Query(Criteria.where("id_user").is(id_user));
            if(this.mongoTemplate.exists(query, D1001_UserModel.class)) {
                List<D1001_UserModel> d1001 = this.mongoTemplate.find(query, D1001_UserModel.class);
                List<D1002_UserProfileModel> d1002 = this.mongoTemplate.find(query, D1002_UserProfileModel.class);
                List<D1003_UserWalletModel> d1003 = this.mongoTemplate.find(query, D1003_UserWalletModel.class);
                List<D1004_UserPoinHistoryModel> d1004 = this.mongoTemplate.find(query, D1004_UserPoinHistoryModel.class);
                
                Map<String, Object> finaresmodel = new HashMap<String, Object>();
                finaresmodel.put("id_user", d1001.get(0).getId_user());

                finaresmodel.put("fullname", d1002.get(0).getFullname());
                finaresmodel.put("nickname", d1002.get(0).getNickname());

                finaresmodel.put("username", d1001.get(0).getUsername());
                finaresmodel.put("email", d1001.get(0).getEmail());
                finaresmodel.put("tlp", d1001.get(0).getTlp());
                finaresmodel.put("password", d1001.get(0).getPassword());

                finaresmodel.put("jk", d1002.get(0).getJk());
                finaresmodel.put("alamat", d1002.get(0).getAlamat());
                finaresmodel.put("foto", d1002.get(0).getFoto());

                finaresmodel.put("roles", d1001.get(0).getRoles());
                finaresmodel.put("token", d1001.get(0).getToken());
                finaresmodel.put("status", d1001.get(0).getStatus());
            
                finaresmodel.put("create_at", d1001.get(0).getCreated_at());
                finaresmodel.put("update_at", d1001.get(0).getUpdated_at());

                finaresmodel.put("poin", d1003.get(0).getPoin());
                finaresmodel.put("updated_at", d1003.get(0).getUpdated_at());
                finaresmodel.put("poin_history", d1004);

                Map<String, Object> finares = new HashMap<String, Object>();
                finares.put("message", "success");
                finares.put("data", finaresmodel);
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                Map<String, Object> finares = new HashMap<String, Object>();
                finares.put("error", 2);
                finares.put("message", "user not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            // log.error("error get user detail by id : " + id_user);
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to get user detail data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @Cacheable(value = "userCache", key = "#id")
    public ResponseEntity<Object> getAll() {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("data", this.mongoTemplate.findAll(D1001_UserModel.class));
            finares.put("message", "success");
            finares.put("success", 1);
            return this.res.generateResponse(finares, 200);
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to get all user data!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "userCache", allEntries = true)
    public ResponseEntity<Object> save(D1001_UserModel model) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query1 = new Query(Criteria.where("username").is(model.getUsername().toString()));
            Query query2 = new Query(Criteria.where("email").is(model.getEmail().toString()));

            if(this.mongoTemplate.exists(query1, D1001_UserModel.class) || this.mongoTemplate.exists(query2, D1001_UserModel.class)) {
                finares.put("error", 2);
                finares.put("message", "username or email already exist!");
                return this.res.generateResponse(finares, 200);
            }
            else {
                long id_user = this.ids.getId_user(this.mongoTemplate); //this.getID();
                model.setId_user(id_user);
                model.setStatus(2);
                model.setToken(this.fun.random(5));
                model.setCreated_at(fun.getDate());
                model.setUpdated_at("");
                mongoTemplate.save(model);
                
                D1002_UserProfileModel d1002 = new D1002_UserProfileModel(
                    id_user, 
                    "", 
                    "", 
                    "", 
                    "", 
                    ""
                );
                mongoTemplate.save(d1002);

                D1003_UserWalletModel d1003 = new D1003_UserWalletModel(
                    id_user, 
                    0, 
                    this.fun.getDate()
                );
                mongoTemplate.save(d1003);

                D1004_UserPoinHistoryModel d1004 = new D1004_UserPoinHistoryModel(
                    id_user, 
                    "Created Saldo", 
                    "created", 
                    this.fun.getDate()
                );
                mongoTemplate.save(d1004);

                D1005_PersonalAccessTokensModel d1005 = new D1005_PersonalAccessTokensModel(
                    id_user, 
                    "D1001_UserModel", 
                    model.getUsername().toString(), 
                    this.fun.random(10),
                    this.token.abilities(model.getRoles()), 
                    "", 
                    this.fun.getFutureDate(30), 
                    this.fun.getDate(), 
                    this.fun.getDate()
                );
                mongoTemplate.save(d1005);

                long kode_otp = Long.parseLong(this.fun.randomNumber(6));
                D0001_SistemOTPModel d0001 = new D0001_SistemOTPModel(
                    this.ids.getId_otp(this.mongoTemplate),
                    id_user,
                    kode_otp
                );
                mongoTemplate.save(d0001);

                String user_role = this.userRole(model.getRoles());
                String user_status = this.userStatus(model.getStatus());

                Map<String, Object> finaresmodel = new HashMap<String, Object>();
                finaresmodel.put("id_user", model.getId_user());
                finaresmodel.put("username", model.getUsername());
                finaresmodel.put("email", model.getEmail());
                finaresmodel.put("no_telpon", model.getTlp());
                finaresmodel.put("password", model.getPassword());
                finaresmodel.put("token", model.getToken());
                finaresmodel.put("role", user_role);
                finaresmodel.put("status", user_status);
                finaresmodel.put("kode_otp", kode_otp);
                finaresmodel.put("created_at", model.getCreated_at());

                finares.put("message", "success save user data");
                finares.put("data", finaresmodel);
                finares.put("success", 1);
                return this.res.generateResponse(finares, 201);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to create user!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "userotpCache", allEntries = true)
    public ResponseEntity<Object> confirmOTP(String user, long kodeotp) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query1 = new Query(
                new Criteria().orOperator(Criteria.where("username").is(user), Criteria.where("email").is(user))
            );

            if(this.mongoTemplate.exists(query1, D1001_UserModel.class)) {
                List<D1001_UserModel> d1001 = this.mongoTemplate.find(query1, D1001_UserModel.class);

                Query query2 = new Query();
                query2.addCriteria(Criteria.where("id_user").is(d1001.get(0).getId_user()));
                List<D0001_SistemOTPModel> d0001 = this.mongoTemplate.find(query2, D0001_SistemOTPModel.class);
                if(d0001.get(0).getKode_otp() == kodeotp) {
                    Update update = new Update().set("status", 1)
                                                .set("updated_at", this.fun.getDate());
                    this.mongoTemplate.updateFirst(query2, update, D1001_UserModel.class);

                    finares.put("message", "success confirmation otp");
                    finares.put("data", update);
                    finares.put("success", 1);
                    return this.res.generateResponse(finares, 200);
                }
                else {
                    finares.put("error", 2);
                    finares.put("message", "fail confirmation otp! try again!");
                    return this.res.generateResponse(finares, 200);
                }
            }
            else {
                finares.put("message", "user not found");
                finares.put("error", 2);
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to fetch and confirm otp!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "userCache", allEntries = true)
    public ResponseEntity<Object> update(long id_user, String tlp, String password, String fullname, String nickname, String jk, String alamat, String foto) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_user").is(id_user));
            if(this.mongoTemplate.exists(query, D1001_UserModel.class)) {
                Update ud1001 = new Update().set("tlp", tlp)
                                            .set("password", password)
                                            .set("updated_at", this.fun.getDate());
                this.mongoTemplate.updateFirst(query, ud1001, D1001_UserModel.class);

                Update ud1002 = new Update().set("fullname", fullname)
                                            .set("nickname", nickname)
                                            .set("jk", jk)
                                            .set("alamat", alamat)
                                            .set("foto", foto)
                                            .set("updated_at", this.fun.getDate());
                this.mongoTemplate.updateFirst(query, ud1002, D1002_UserProfileModel.class);

                Map<String, Object> resmodel = new HashMap<String, Object>();
                resmodel.put("0", ud1001);
                resmodel.put("1", ud1002);

                finares.put("message", "success update user data");
                finares.put("data", resmodel);
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "fail to update user data!");
                return this.res.generateResponse(finares, 200);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to update user!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "userCache", allEntries = true)
    public ResponseEntity<Object> softDelete(long id_user) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_user").is(id_user));
            if(this.mongoTemplate.exists(query, D1001_UserModel.class)) {
                Update update = new Update().set("status", 0)
                                            .set("updated_at", this.fun.getDate());
                this.mongoTemplate.updateFirst(query, update, D1001_UserModel.class);

                finares.put("message", "success soft delete user");
                finares.put("data", update);
                finares.put("success", 1);
                return this.res.generateResponse(finares, 200);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "user not found!");
                return this.res.generateResponse(finares, 404);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to soft delete user!");
            return this.res.generateResponse(finares, 500);
        }
    }

    @CacheEvict(value = "userCache", allEntries = true)
    public ResponseEntity<Object> hardDelete(long id_user) {
        try {
            Map<String, Object> finares = new HashMap<String, Object>();
            Query query = new Query(Criteria.where("id_user").is(id_user));
            if(this.mongoTemplate.exists(query, D1001_UserModel.class)) {
                List<D1001_UserModel> d1001 = this.mongoTemplate.find(query, D1001_UserModel.class);
                this.mongoTemplate.remove(query , D0001_SistemOTPModel.class);
                this.mongoTemplate.remove(query , D1001_UserModel.class);
                this.mongoTemplate.remove(query , D1002_UserProfileModel.class);
                this.mongoTemplate.remove(query , D1003_UserWalletModel.class);
                this.mongoTemplate.remove(query , D1004_UserPoinHistoryModel.class);
                this.mongoTemplate.remove(query , D1005_PersonalAccessTokensModel.class);

                finares.put("data", d1001);
                finares.put("message", "success hard delete user!");
                finares.put("success", 1);
                return this.res.generateResponse(finares, 500);
            }
            else {
                finares.put("error", 2);
                finares.put("message", "user not found!");
                return this.res.generateResponse(finares, 400);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Map<String, Object> finares = new HashMap<String, Object>();
            finares.put("error", 1);
            finares.put("message", "fail to hard delete user!");
            return this.res.generateResponse(finares, 500);
        }
    }
}
