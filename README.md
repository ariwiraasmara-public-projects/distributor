# Distributor
Sistem Distribusi Barang Jenis B2B (Distributor)

# Thanks To
- Gitlab (https://gitlab.com), as version control
- Springboot (https://start.spring.io/), as backend
- Flutter (https://flutter.dev/), as frontend
- MongoDB (https://www.mongodb.com/), as database
- Redis (https://redis.io/), as cache
- Elastissearch (https://www.elastic.co/elasticsearch), as query database

# Copyright @ Miftah Chaidir Basyaib & Ari Wiraasmara