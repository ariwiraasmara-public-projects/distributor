class D1001usermodel {

    int? idUser;
    String? username;
    String? email;
    String? tlp;
    String? password;
    int? roles;
    String? token;
    int? status;
    String? createdAt;
    String? updatedAt;

    D1001usermodel(this.idUser, this.username, this.email, this.tlp, this.password, this.roles, this.token, this.status, this.createdAt, this.updatedAt);
    D1001usermodel.defaultValues() : this(0, "__null__", "__null__", "__null__", "__null__", 0, "__null__", 0, "__null__", "__null__");
    D1001usermodel.storeUser(String this.username, String this.email, String this.tlp, String this.password);
    D1001usermodel.updateUser(int this.idUser, String this.tlp, String this.password);

    int? get getIdUser => idUser;
    set setIdUser(int? idUser) => this.idUser = idUser;

    String? get getUsername => username;
    set setUsername(String? username) => this.username = username;

    String? get getEmail => email;
    set setEmail(String? email) => this.email = email;

    String? get getTlp => tlp;
    set setTlp(String? tlp) => this.tlp = tlp;

    String? get getPassword => password;
    set setPassword(String? password) => this.password = password;

    int? get getRoles => roles;
    set setRoles(int? roles) => this.roles = roles;

    String? get getToken => token;
    set setToken(String? token) => this.token = token;

    int? get getStatus => status;
    set setStatus(int? status) => this.status = status;

    String? get getCreatedAt => createdAt;
    set setCreatedAt(String? createdAt) => this.createdAt = createdAt;

    String? get getUpdatedAt => updatedAt;
    set setUpdatedAt(String? updatedAt) => this.updatedAt = updatedAt;
}