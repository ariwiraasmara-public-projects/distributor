class D2003barangmodel {

    int? idBarang;
    String? namaBarang;
    int? jenisBarang;
    int? merkBarang;
    String? satuan;
    int? hargaBeli;
    int? hargaJual;
    int? stok;

    D2003barangmodel(this.idBarang, this.namaBarang, this.jenisBarang, this.merkBarang, this.satuan, this.hargaBeli, this.hargaJual, this.stok);
    D2003barangmodel.defaultValues() : this(0, "__null__", 0, 0, "__null__", 0, 0 , 0);

    int? get getIdBarang => this.idBarang;
    set setIdBarang(int? idBarang) => this.idBarang = idBarang;

    String? get getNamaBarang => this.namaBarang;
    set setNamaBarang(String? namaBarang) => this.namaBarang = namaBarang;

    int? get getJenisBarang => this.jenisBarang;
    set setJenisBarang(int? jenisBarang) => this.jenisBarang = jenisBarang;

    int? get getMerkBarang => this.merkBarang;
    set setMerkBarang(int? merkBarang) => this.merkBarang = merkBarang;

    String? get getSatuan => this.satuan;

    set setSatuan(String? satuan) => this.satuan = satuan;
    int? get getHargaBeli => this.hargaBeli;

    set setHargaBeli(int? hargaBeli) => this.hargaBeli = hargaBeli;
    int? get getHargaJual => this.hargaJual;

    set setHargaJual(int? hargaJual) => this.hargaJual = hargaJual;

    int? get getStok => this.stok;
    set setStok(int? stok) => this.stok = stok;
}