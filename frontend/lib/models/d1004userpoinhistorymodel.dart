class D1004userpoinhistorymodel {

    int? idUser;
    String? deskripsi;
    String? status;
    String? tglHistory;

    D1004userpoinhistorymodel(this.idUser, this.deskripsi, this.status, this.tglHistory);
    D1004userpoinhistorymodel.defaultValues() : this(0, "__null__", "__null__", "__null__");

    int? get getIdUser => idUser;
    set setIdUser(int? idUser) => this.idUser = idUser;

    String? get getDeskripsi => deskripsi;
    set setDeskripsi(String? deskripsi) => this.deskripsi = deskripsi;

    String? get getStatus => status;
    set setStatus(String? status) => this.status = status;

    String? get getTglHistory => tglHistory;
    set setTglHistory(String? tglHistory) => this.tglHistory = tglHistory;
}