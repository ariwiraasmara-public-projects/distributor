class D3002distributorprofilemodel {

    String? idDistributor;
    String? deskripsi;
    String? alamat;
    String? foto;

    D3002distributorprofilemodel(this.idDistributor, this.deskripsi, this.alamat, this.foto);
    D3002distributorprofilemodel.defaultValues() : this("__null__", "__null__", "__null__", "__null__");

    String? get getIdDistributor => idDistributor;
    set setIdDistributor(String? idDistributor) => this.idDistributor = idDistributor;

    String? get getDeskripsi => deskripsi;
    set setDeskripsi(String?  deskripsi) => this.deskripsi = deskripsi;

    String? get getAlamat => alamat;
    set setAlamat(String?  alamat) => this.alamat = alamat;

    String? get getFoto => foto;
    set setFoto(String?  foto) => this.foto = foto;
}