class D4001transaksimodel {

    String? idTransaksi;
    String? invoice;
    String? nomorTransaksi;
    String? distributor;
    int? user;
    int? barang;
    int? jumlahBarang;
    int? status; // 0 = Nothing; 1 = Done and Bought; 2 = Pending, In Basket; 3 = Cancelled
    String? updateAt;

    D4001transaksimodel(this.idTransaksi, this.invoice, this.nomorTransaksi, this.distributor, this.user, this.barang, this.jumlahBarang, this.status, this.updateAt);
    D4001transaksimodel.defaultValues() : this("__null__", "__null__", "__null__", "__null__", 0, 0, 0, 0, "__null__");

    String? get getIdTransaksi => idTransaksi;
    set setIdTransaksi(String? idTransaksi) => this.idTransaksi = idTransaksi;

    String? get getInvoice => invoice;
    set setInvoice(String? invoice) => this.invoice = invoice;

    String? get getNomorTransaksi => nomorTransaksi;
    set setNomorTransaksi(String? nomorTransaksi) => this.nomorTransaksi = nomorTransaksi;

    String? get getDistributor => distributor;
    set setDistributor(String? distributor) => this.distributor = distributor;

    int? get getUser => user;
    set setUser(int? user) => this.user = user;

    int? get getBarang => barang;
    set setBarang(int? barang) => this.barang = barang;

    int? get getJumlahBarang => jumlahBarang;
    set setJumlahBarang(int?  jumlahBarang) => this.jumlahBarang = jumlahBarang;

    int? get getStatus => status;
    set set3(int? status) => this.status = status;

    String? get getUpdateAt => updateAt;
    set setUpdateAt(String? updateAt) => this.updateAt = updateAt;
}