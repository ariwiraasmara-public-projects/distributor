class D1003userwalletmodel {

    int? idUser;
    double? poin;
    String? updatedAt;

    D1003userwalletmodel(this.idUser, this.poin, this.updatedAt);
    D1003userwalletmodel.defaultValues() : this(0, 0, "__null__");

    int? get getIdUser => idUser;
    set setIdUser(int? idUser) => this.idUser = idUser;

    double? get getPoin => poin;
    set setPoin(double? poin) => this.poin = poin;

    String? get getUpdatedAt => updatedAt;
    set setUpdatedAt(String? updatedAt) => this.updatedAt = updatedAt;
}