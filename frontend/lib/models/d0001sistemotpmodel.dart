class D0001sistemotpmodel {

    int? idOtp;
    int? idUser;
    int? kodeOtp;

    D0001sistemotpmodel(this.idOtp, this.idUser, this.kodeOtp);
    D0001sistemotpmodel.defaultValues() : this(0, 0, 123456);

    int? get getIdOtp => idOtp;
    set setIdOtp(int? idOtp) => this.idOtp = idOtp;

    int? get getIdUser => idUser;
    set setIdUser(int? idUser) => this.idUser = idUser;

    int? get getKodeOtp => kodeOtp;
    set setKodeOtp(int? kodeOtp) => this.kodeOtp = kodeOtp;

}