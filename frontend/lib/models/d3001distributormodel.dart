class D3001distributormodel {

    String? idDistributor;
    int? user;
    String? namaDistributor;
    double? longitude;
    double? latitude;
    String? createdAt;
    String? updatedAt;
    String? deletedAt;

    D3001distributormodel(this.idDistributor, this.user, this.namaDistributor, this.longitude, this.latitude, this.createdAt, this.updatedAt, this.deletedAt);
    D3001distributormodel.defaultValues() : this("__null__", 0, "__null__", 0 , 0 , "__null__", "__null__", "__null__");
    D3001distributormodel.storeDistributor(this.namaDistributor);

    String? get getIdDistributor => idDistributor;
    set setIdDistributor(String? idDistributor) => this.idDistributor = idDistributor;

    int? get getUser => user;
    set setUser(int? user) => this.user = user;

    String? get getNamaDistributor => namaDistributor;
    set setNamaDistributor(String? namaDistributor) => this.namaDistributor = namaDistributor;

    double? get getLongitude => longitude;
    set setLongitude(double? longitude) => this.longitude = longitude;

    double? get getLatitude => latitude;
    set setLatitude(double? latitude) => this.latitude = latitude;

    String? get getCreatedAt => createdAt;
    set setCreatedAt(String? createdAt) => this.createdAt = createdAt;

    String? get getUpdatedAt => updatedAt;
    set setUpdatedAt(String? updatedAt) => this.updatedAt = updatedAt;

    String? get getDeletedAt => deletedAt;
    set setDeletedAt(String? deletedAt) => this.deletedAt = deletedAt;
}