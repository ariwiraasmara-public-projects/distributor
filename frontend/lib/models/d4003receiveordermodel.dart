class D4003receiveordermodel {

    String? idReceiveorder;
    String? invoice;
    String? nomorTransaksi;
    String? distributor;
    int? byUser;
    int? barang;
    int? jumlahBarang;
    String? tglSampai;
    int? isBarangExist; // 0 = Not Exist; 1 = Exist; 2 = On Hold, Sedang dikirim

    D4003receiveordermodel(this.idReceiveorder, this.invoice, this.nomorTransaksi, this.distributor, this.byUser, this.barang, this.jumlahBarang, this.tglSampai, this.isBarangExist);
    D4003receiveordermodel.defaultValues() : this("__null__", "__null__", "__null__", "__null__", 0 , 0, 0, "__null__", 0);

    String? get getIdReceiveorder => idReceiveorder;
    set setIdReceiveorder(String? idReceiveorder) => this.idReceiveorder = idReceiveorder;

    String? get getInvoice => invoice;
    set setInvoice(String? invoice) => this.invoice = invoice;

    String? get getNomorTransaksi => nomorTransaksi;
    set setNomorTransaksi(String? nomorTransaksi) => this.nomorTransaksi = nomorTransaksi;

    String? get getDistributor => distributor;
    set setDistributor(String? distributor) => this.distributor = distributor;

    int? get getByUser => byUser;
    set setByUser( byUser) => this.byUser = byUser;

    int? get getBarang => barang;
    set setBarang( barang) => this.barang = barang;

    int? get getJumlahBarang => jumlahBarang;
    set setJumlahBarang( jumlahBarang) => this.jumlahBarang = jumlahBarang;

    String? get getTglSampai => tglSampai;
    set setTglSampai(String? tglSampai) => this.tglSampai = tglSampai;

    int? get getIsBarangExist => 2;
    set setIsBarangExist(int? isBarangExist) => this.isBarangExist = isBarangExist;
}