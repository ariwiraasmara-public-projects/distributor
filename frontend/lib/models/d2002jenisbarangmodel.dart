class D2002jenisbarangmodel {

    int? idJenisbarang;
    String? jenisBarang;

    D2002jenisbarangmodel(this.idJenisbarang, this.jenisBarang);
    D2002jenisbarangmodel.defaultValues() : this(0, "__null__");

    int? get getIdJenisbarang => this.idJenisbarang;
    set setIdJenisbarang(int? idJenisbarang) => this.idJenisbarang = idJenisbarang;

    String? get getJenisBarang => this.jenisBarang;
    set setJenisBarang(String? jenisBarang) => this.jenisBarang = jenisBarang;
}