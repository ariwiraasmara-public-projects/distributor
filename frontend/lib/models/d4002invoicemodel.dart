class D4002invoicemodel {

    String? idInvoice;
    String? tglInvoice;
    String? tujuan;
    String? tglKirim;

    D4002invoicemodel(this.idInvoice, this.tglInvoice, this.tujuan, this.tglKirim);
    D4002invoicemodel.defaultValues() : this("__null__", "__null__", "__null__", "__null__");

    String? get getIdInvoice => idInvoice;
    set setIdInvoice(String? idInvoice) => this.idInvoice = idInvoice;

    String? get getTglInvoice => tglInvoice;
    set setTglInvoice(String? tglInvoice) => this.tglInvoice = tglInvoice;

    String? get getTujuan => tujuan;
    set setTujuan(String? tujuan) => this.tujuan = tujuan;

    String? get getTglKirim => tglKirim;
    set setTglKirim(String? tglKirim) => this.tglKirim = tglKirim;
}