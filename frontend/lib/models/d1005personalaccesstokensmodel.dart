class D1005personalaccesstokensmodel {

    int? idUser;
    String? tokenableType;
    String? name;
    String? token;
    String? abilities;
    String? lastusedAt;
    String? expiresAt;
    String? createdAt;
    String? updatedAt;

    D1005personalaccesstokensmodel(this.idUser, this.tokenableType, this.name, this.token, this.abilities, this.expiresAt, this.updatedAt, this.createdAt);
    D1005personalaccesstokensmodel.defaultValues() : this(0, "__null__", "__null__", "__null__", "__null__", "__null__", "__null__", "__null__");

    int? get getIdUser => idUser;
    set setIdUser(int? idUser) => this.idUser = idUser;

    String? get getTokenableType => tokenableType;
    set setTokenableType(String? tokenableType) => this.tokenableType = tokenableType;

    String? get getName => name;
    set setName(String? name) => this.name = name;

    String? get getToken => token;
    set setToken(String? token) => this.token = token;

    String? get getAbilities => abilities;
    set setAbilities(String? abilities) => this.abilities = abilities;

    String? get getLastusedAt => lastusedAt;
    set setLastusedAt(String? lastusedAt) => this.lastusedAt = lastusedAt;

    String? get getExpiresAt => expiresAt;
    set setExpiresAt(String? expiresAt) => this.expiresAt = expiresAt;

    String? get getCreatedAt => createdAt;
    set setCreatedAt(String? createdAt) => this.createdAt = createdAt;

    String? get getUpdatedAt => updatedAt;
    set setUpdatedAt(String? updatedAt) => this.updatedAt = updatedAt;
}