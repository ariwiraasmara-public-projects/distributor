class D2001merkbarangmodel {

    int? idMerkbarang;
    String? merkBarang;
    String? deskripsi;

    D2001merkbarangmodel(this.idMerkbarang, this.merkBarang, this.deskripsi);
    D2001merkbarangmodel.defaultValues() : this(0, "__null__", "__null__");

    int? get getIdMerkbarang => idMerkbarang;
    set setIdMerkbarang(int? idMerkbarang) => this.idMerkbarang = idMerkbarang;

    String? get getMerkBarang => merkBarang;
    set setMerkBarang(String? merkBarang) => this.merkBarang = merkBarang;

    String? get getDeskripsi => deskripsi;
    set setDeskripsi(String? deskripsi) => this.deskripsi = deskripsi;
}