class D1002userprofilemodel {

    int? idUser;
    String? fullname;
    String? nickname;
    String? jk;
    String? alamat;
    String? foto;

    D1002userprofilemodel(this.idUser, this.fullname, this.nickname, this.jk, this.alamat, this.foto);
    D1002userprofilemodel.defaultValues() : this(0, "__null__", "__null__", "__null__", "__null__", "__null__");

    int? get getIdUser => idUser;
    set setIdUser(int? idUser) => this.idUser = idUser;

    String? get getFullname => fullname;
    set setFullname(String? fullname) => this.fullname = fullname;

    String? get getNickname => nickname;
    set setNickname(String? nickname) => this.nickname = nickname;

    String? get getJk => jk;
    set setJk(String? jk) => this.jk = jk;

    String? get getAlamat => alamat;
    set setAlamat(String? alamat) => this.alamat = alamat;

    String? get getFoto => foto;
    set setFoto(String? foto) => this.foto = foto;

}