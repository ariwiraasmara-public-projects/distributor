class D3003distributorstaffmodel {

    int? idUser;
    String? idDistributor;
    String? jabatan;

    D3003distributorstaffmodel(this.idUser, this.idDistributor, this.jabatan);
    D3003distributorstaffmodel.defaultValues() : this(0, "__null__", "__null__");

    int? get getIdUser => idUser;
    set setIdUser(int? idUser) => this.idUser = idUser;

    String? get getIdDistributor => idDistributor;
    set setIdDistributor(String? idDistributor) => this.idDistributor = idDistributor;

    String? get getJabatan => jabatan;
    set setJabatan(String? jabatan) => this.jabatan = jabatan;
}