import 'package:flutter/material.dart';

class Mappbarlogout extends StatelessWidget {
    const Mappbarlogout({super.key});

    @override
    Widget build(BuildContext context) {
        return AppBar(
            shadowColor: const Color.fromRGBO(0, 0, 0, 1),
            backgroundColor: const Color.fromRGBO(62, 48, 143, 1),
            title: const Text('Login', style: TextStyle(fontFamily: 'Georgia', fontWeight: FontWeight.bold, color: Color.fromRGBO(255, 255, 255, 1)),),
            centerTitle: true,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(
                    bottom: Radius.circular(50),
                ),
            ),
        );
    }
}