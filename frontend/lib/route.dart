import 'package:flutter/material.dart';
import 'package:frontend_distributor/views/p404.dart';
import 'package:frontend_distributor/views/logout/login.dart';
import 'package:frontend_distributor/views/logout/signup.dart';
import 'package:frontend_distributor/views/login/dashboard.dart';
import 'package:frontend_distributor/views/login/barang/createedit.dart' as add_barang;
import 'package:frontend_distributor/views/login/jenisbarang/createedit.dart' as add_jenisbarang;
import 'package:frontend_distributor/views/login/merkbarang/createedit.dart' as add_merkbarang;

    MaterialPageRoute toPage(String? to) {
        if(to == 'login') {
            return MaterialPageRoute(
                builder: (context) => const LoginPage()
            );
        }
        else if(to == 'signup') {
            return MaterialPageRoute(
                builder: (context) => const SignupPage()
            );
        }
        else if(to == 'dashboard') {
            return MaterialPageRoute(
                builder: (context) => const DashboardPage()
            );
        }
        else if(to == 'add barang') {
            return MaterialPageRoute(
                builder: (context) => const add_barang()
            );
        }
        else if(to == 'add merk barang') {
            return MaterialPageRoute(
                builder: (context) => const add_merkbarang()
            );
        }
        else if(to == 'add jenis barang') {
            return MaterialPageRoute(
                builder: (context) => const add_jenisbarang()
            );
        }
        else {
            return MaterialPageRoute(
                builder: (context) => const P404Page()
            );
        } 
        
    }

