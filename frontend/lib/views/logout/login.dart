import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:frontend_distributor/api.dart';
import 'package:frontend_distributor/route.dart' as route;
import 'package:logger/logger.dart';
import 'package:frontend_distributor/mwidgets/mappbarlogout.dart' as mappbar;

class LoginPage extends StatefulWidget {
    const LoginPage({super.key});

    @override
    State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
    final _formKey = GlobalKey<FormState>();
    String user = "";
    String pass = "";
    var log = Logger();
    var api = Api();

    @override 
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                shadowColor: const Color.fromRGBO(0, 0, 0, 1),
                backgroundColor: const Color.fromRGBO(62, 48, 143, 1),
                title: const Text('Login', style: TextStyle(fontFamily: 'Georgia', fontWeight: FontWeight.bold, color: Color.fromRGBO(255, 255, 255, 1)),),
                centerTitle: true,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.vertical(
                        bottom: Radius.circular(50),
                    ),
                ),
            ),
            body: Container(
                margin: const EdgeInsets.all(30),
                decoration: BoxDecoration(
                    color: const Color.fromRGBO(255, 255, 255, 1),
                    border: Border.all(color: const Color.fromRGBO(0, 0, 0, 0), style: BorderStyle.solid, width: 1),
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    boxShadow: const [
                        BoxShadow(
                            color: Color.fromRGBO(125, 125, 125, 0.7),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                        ),
                    ],
                ),
                child: Form(
                    key: _formKey,
                    child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                            children: [
                                TextFormField(
                                    decoration: const InputDecoration(labelText: 'Email'),
                                    onChanged: (value) => user = value,
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Email must be filled';
                                      }
                                      return null;
                                    },
                                ),
                                const SizedBox(height: 20.0),
                                TextFormField(
                                    decoration: const InputDecoration(labelText: 'Password'),
                                    obscureText: true,
                                    onChanged: (value) => pass = value,
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Password must be filled';
                                      }
                                      return null;
                                    },
                                ),
                                const SizedBox(height: 20.0),
                                ElevatedButton(
                                    onPressed: () async {
                                      if (_formKey.currentState!.validate()) {
                                          // Lakukan proses login di sini
                                          // log.d('Username: $user, Password: $pass');
                                          try {
                                              final body = {
                                                  "user" : user,
                                                  "pass" : pass
                                              };

                                              // final uri = Uri.https("http://localhost:8080/api/user/login");
                                              // final response = await http.post(
                                              //     uri, 
                                              //     body: jsonEncode(body),
                                              //     headers: {
                                              //       "Access-Control-Allow-Origin": "*",
                                              //       'Content-Type': 'application/json',
                                              //     }
                                              // );
                                              // log.d('Username: $user, Password: $pass');
                                              // log.d('response $response.statusCode');

                                              if( (user == "akairo") && (pass == "@14iR0") ) {
                                                  SharedPreferences prefs = await SharedPreferences.getInstance();
                                                  prefs.setString('islogin', "1");
                                                  prefs.setString('--dx1--', "akairo");
                                                  prefs.setString('--dx2--', "akairogreyjack@gmail.com");
                                                  prefs.setString('--dx3--', "5");
                                                  prefs.setString('--dx4--', "IDD@0000000000000001");
                                                  Navigator.push(context, route.toPage("dashboard"));
                                              }
                                              // var result = await api.userLogin(user, pass);
                                              // log.d('result $result'); // Output: status kode dari server
                                              // Lakukan tindakan selanjutnya berdasarkan status kode
                                          } catch (error) {
                                              log.e(error.toString());
                                              log.e('Error saat login: $error');
                                              // Tampilkan pesan error kepada pengguna
                                          }
                                      }
                                    },
                                    child: Text('Login', style: TextStyle(color: Color.fromRGBO(255, 255, 255, 1)),),
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: Color.fromARGB(255, 105, 94, 255),
                                      
                                    ),
                                ),
                                const SizedBox(height: 16.0),
                                ElevatedButton(
                                    onPressed: () {
                                        // final route = MaterialPageRoute(
                                        //     builder: (context) => const SignupPage()
                                        // );
                                        // Navigator.push(context, route);
                                        Navigator.push(context, route.toPage("signup"));
                                    },
                                    child: Text('Sign Up', style: TextStyle(color: Color.fromRGBO(255, 255, 255, 1)),),
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: Color.fromARGB(255, 4, 0, 255),
                                      
                                    ),
                                ),
                                const SizedBox(height: 16.0),
                                ElevatedButton(
                                    onPressed: () {
                                      // Lakukan proses lupa password
                                    },
                                    child: Text('Forgot Password', style: TextStyle(color: Color.fromRGBO(255, 255, 255, 1)),),
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: Color.fromARGB(255, 160, 0, 0),
                                      
                                    ),
                                ),
                                const SizedBox(height: 50.0),
                                const Center(
                                    child: Align(
                                        alignment: Alignment.bottomCenter,
                                        child: Text('Copyright @ Syahri Ramadhan Wiraasmara (ARI)', 
                                                    style: TextStyle(fontFamily: 'Georgia', 
                                                    fontWeight: FontWeight.bold,
                                                    color: Color.fromRGBO(0, 0, 0, 1),),
                                        )
                                    ),
                                )
                            ],
                        ),
                    ),
                ),
            ),
        );
    }
}