import 'package:flutter/material.dart';

class SignupPage extends StatefulWidget {
    const SignupPage({super.key});

    @override
    State<SignupPage> createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
    final _formKey = GlobalKey<FormState>();
    String username = '';
    String email = '';
    String tlp = '';
    String password = '';
    String namadistributor = '';

    @override 
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                shadowColor: const Color.fromRGBO(0, 0, 0, 1),
                backgroundColor: const Color.fromRGBO(62, 48, 143, 1),
                title: const Text('Sign Up', style: TextStyle(fontFamily: 'Georgia', fontWeight: FontWeight.bold, color: Color.fromRGBO(255, 255, 255, 1)),),
                centerTitle: true,
                iconTheme: const IconThemeData(
                    color: Colors.white, //change your color here
                ),
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                    bottom: Radius.circular(50),
                  ),
                ),
            ),
            body: Container(
                margin: const EdgeInsets.all(30),
                decoration: BoxDecoration(
                    color: const Color.fromRGBO(255, 255, 255, 1),
                    border: Border.all(color: const Color.fromRGBO(0, 0, 0, 0), style: BorderStyle.solid, width: 1),
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    boxShadow: const [
                        BoxShadow(
                            color: Color.fromRGBO(125, 125, 125, 0.7),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                        ),
                    ],
                ),
                child: Form(
                    key: _formKey,
                    child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                            children: [
                                // username
                                TextFormField(
                                  decoration: const InputDecoration(labelText: 'Username'),
                                  onChanged: (value) => username = value,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                        return 'Username must be filled';
                                    }
                                    return null;
                                  },
                                ),
                                // email
                                TextFormField(
                                  decoration: const InputDecoration(labelText: 'Email'),
                                  onChanged: (value) => email = value,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                        return 'Email must be filled';
                                    }
                                    return null;
                                  },
                                ),
                                // tlp
                                TextFormField(
                                  decoration: const InputDecoration(labelText: 'Phone Number (with Country Code)'),
                                  onChanged: (value) => tlp = value,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                        return 'Phone Number (with Country Code) must be filled';
                                    }
                                    return null;
                                  },
                                ),
                                // password
                                TextFormField(
                                  decoration: const InputDecoration(labelText: 'Password'),
                                  onChanged: (value) => password = value,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                        return 'Password must be filled';
                                    }
                                    return null;
                                  },
                                ),
                                // distributor
                                TextFormField(
                                  decoration: const InputDecoration(labelText: 'Distributor Name'),
                                  onChanged: (value) => namadistributor = value,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                        return 'Distributor Name must be filled';
                                    }
                                    return null;
                                  },
                                ),
                                const SizedBox(height: 50.0),
                                const Center(
                                    child: Align(
                                        alignment: Alignment.center,
                                        child: Text('Copyright @ Syahri Ramadhan Wiraasmara (ARI)', 
                                                    style: TextStyle(fontFamily: 'Georgia', 
                                                    fontWeight: FontWeight.bold,
                                                    color: Color.fromRGBO(0, 0, 0, 1),),
                                        )
                                    ),
                                )
                            ],
                        )
                    )
                )
            )
        );
    }
}