// Terdapat 

// Terdapat 3 Tab:
// Tab 1 - List Barang dan detilnya, user dapat mencari barang berdasarkan :
// - Nama Barang
// - Jenis Barang
// - Merk Barang
// 
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:frontend_distributor/api.dart';
import 'package:frontend_distributor/route.dart' as route;
import 'package:logger/logger.dart';

class DashboardPage extends StatefulWidget {
    const DashboardPage({super.key});

    @override
    State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
    // SharedPreferences prefs = SharedPreferences.getInstance();
    var log = Logger();
    var api = Api();
    // List items = api.barangAll();
    List items = [];
    // int islogin = int.parse(prefs.get("islogin"));
    int roles = 5;
    // log.d("");

    AppBar headAppbar1234() {
        return AppBar(
            shadowColor: const Color.fromRGBO(0, 0, 0, 1),
            backgroundColor: const Color.fromRGBO(62, 48, 143, 1),
            title: const Text('', style: TextStyle(fontFamily: 'Georgia', fontWeight: FontWeight.bold, color: Color.fromRGBO(255, 255, 255, 1)),),
            centerTitle: false,
            bottom: const TabBar(
                tabs: <Widget>[
                    Tab(text: "Goods",),
                    Tab(text: "Brands"),
                    Tab(text: "Types"),
                ],
            ),
        );
    }

    TabBarView bodyTabbar1234() {
        return const TabBarView(
            children: <Widget>[
                Center(
                    child: Text("List of All Goods"),
                ),
                Center(
                    child: Text("List of Brands of Goods"),
                ),
                Center(
                    child: Text("List of Types of Goods"),
                ),
            ],
        );
    }



    AppBar headAppbar567() {
        return AppBar(
            shadowColor: const Color.fromRGBO(0, 0, 0, 1),
            backgroundColor: const Color.fromRGBO(62, 48, 143, 1),
            title: const Text('', style: TextStyle(fontFamily: 'Georgia', fontWeight: FontWeight.bold, color: Color.fromRGBO(255, 255, 255, 1)),),
            centerTitle: false,
        );
    }

    

    ListView bodyDashboard567() {
        return ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, index) {
                return const ListTile(
                    title: Text("[Merk_Barang] [Nama_Barang]"),
                    subtitle: Text("[Jenis_Barang]\n"
                                   "Stok: [Stok]\n"
                                   "Purchase Price: Rp. [Harga_Beli]\n"
                                   "Selling Price: Rp. [Harga_Jual]"),   
                );
            }
        ); 
    }

    final List<FloatingActionButton> fabs=[
        FloatingActionButton(child: const Icon(Icons.access_time),onPressed: () {

        },),
        FloatingActionButton(child: const Icon(Icons.account_balance),onPressed: () {

        },),
        FloatingActionButton(child: const Icon(Icons.add_alert),onPressed: () {

        },)
    ];

    
    @override 
    Widget build(BuildContext context) {
        int fabindex = 1;

        if(roles > 0 && roles < 5) {
            return Scaffold(
                appBar: headAppbar1234(),
                body: bodyTabbar1234()
            );
        }
        else if(roles > 4) {
            return Scaffold(
                appBar: headAppbar567(),
                body: bodyDashboard567()
            );
        }
        else {
          return Scaffold(
              appBar: headAppbar567(),
              body: const Center(
                  child: Align(
                      alignment: Alignment.center,
                      child: Text('Unauthorized!', 
                                  style: TextStyle(fontFamily: 'Georgia', 
                                  fontWeight: FontWeight.w900,
                                  fontSize: 50,
                                  color: Color.fromRGBO(0, 0, 0, 1),),
                      )
                  ),
              )
          );
        }
    }
}