import 'package:flutter/material.dart';

class CreateeditBarangPage extends StatefulWidget {
    const CreateeditBarangPage({super.key});

    @override
    State<CreateeditBarangPage> createState() => _CreateeditBarangPageState();
}

class _CreateeditBarangPageState extends State<CreateeditBarangPage> {
    final _formKey = GlobalKey<FormState>();

    String namabarang = "";

    const List<String> listjenis = <String>[];
    String jenisbarang = listjenis.first;

    const List<String> listmerk = <String>[];
    String merkbarang = listmerk.first;

    String satuan = '';
    int harga_beli = 0;
    int harga_jual = 0;
    int stok = 0;

    const List<String> list = <String>['One', 'Two', 'Three', 'Four'];
    String dropdownValue = list.first;

    @override 
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                shadowColor: const Color.fromRGBO(0, 0, 0, 1),
                backgroundColor: const Color.fromRGBO(62, 48, 143, 1),
                title: const Text('', style: TextStyle(fontFamily: 'Georgia', fontWeight: FontWeight.bold, color: Color.fromRGBO(255, 255, 255, 1)),),
                centerTitle: false,
            ),
            body: ListView(
                children: [
                    TextFormField(
                        decoration: const InputDecoration(labelText: 'Nama Barang'),
                        onChanged: (value) => namabarang = value,
                        validator: (value) {
                            if (value == null || value.isEmpty) {
                                return 'Nama Barang must be filled';
                            }
                            return null;
                        },
                    ),
                    DropdownButton<String>(
                        value: jenisbarang,
                        icon: const Icon(Icons.arrow_downward),
                        elevation: 16,
                        style: const TextStyle(color: Colors.deepPurple),
                        underline: Container(
                            height: 2,
                            color: Colors.deepPurpleAccent,
                        ),
                        onChanged: (String? value) {
                            // This is called when the user selects an item.
                            setState(() {
                                dropdownValue = value!;
                            });
                        },
                        items: list.map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                            );
                        }).toList(),
                    ),
                    DropdownButton<String>(
                        value: merkbarang,
                        icon: const Icon(Icons.arrow_downward),
                        elevation: 16,
                        style: const TextStyle(color: Colors.deepPurple),
                        underline: Container(
                            height: 2,
                            color: Colors.deepPurpleAccent,
                        ),
                        onChanged: (String? value) {
                            // This is called when the user selects an item.
                            setState(() {
                              dropdownValue = value!;
                            });
                        },
                        items: list.map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                            );
                        }).toList(),
                    ),
                    TextFormField(
                        decoration: const InputDecoration(labelText: 'Satuan'),
                        onChanged: (value) => satuan = value,
                        validator: (value) {
                            if (value == null || value.isEmpty) {
                                return 'Satuan must be filled';
                            }
                            return null;
                        },
                    ),
                    TextFormField(
                        decoration: const InputDecoration(labelText: 'Harga Beli'),
                        onChanged: (value) => harga_beli = value,
                        validator: (value) {
                            if (value == null || value.isEmpty) {
                                return 'Harga Beli must be filled';
                            }
                            return null;
                        },
                    ),
                    TextFormField(
                        decoration: const InputDecoration(labelText: 'Harga Jual'),
                        onChanged: (value) => harga_jual = value,
                        validator: (value) {
                            if (value == null || value.isEmpty) {
                                return 'Harga Jual must be filled';
                            }
                            return null;
                        },
                    ),
                    TextFormField(
                        decoration: const InputDecoration(labelText: 'Stok'),
                        onChanged: (value) => satuan = value,
                        validator: (value) {
                            if (value == null || value.isEmpty) {
                                return 'Stok must be filled';
                            }
                            return null;
                        },
                    ),
                ],
            )
        );
    }
}