import 'package:flutter/material.dart';

class P404Page extends StatefulWidget {
    const P404Page({super.key});

    @override
    State<P404Page> createState() => _P404PageState();
}

class _P404PageState extends State<P404Page> {
    @override 
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                shadowColor: const Color.fromRGBO(88, 88, 88, 0.7),
                backgroundColor: const Color.fromRGBO(62, 48, 143, 1),
                title: const Text('404!', style: TextStyle(fontFamily: 'Georgia', fontWeight: FontWeight.bold, color: Color.fromRGBO(255, 255, 255, 1)),),
                centerTitle: true,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                    bottom: Radius.circular(50),
                  ),
                  
                ),
            ),
            body: const Center(
                child: Align(
                    alignment: Alignment.center,
                    child: Text('Page Not Found!', 
                                style: TextStyle(fontFamily: 'Georgia', 
                                fontWeight: FontWeight.w900,
                                fontSize: 50,
                                color: Color.fromRGBO(0, 0, 0, 1),),
                    )
                ),
            ),
        );
    }
}