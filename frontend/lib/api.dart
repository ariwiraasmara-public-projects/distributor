import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';

class Api {

    final String url = "http://localhost:8080/api/";

    final String confirmOTP = "confirm/otp/";

    final String login = "user/login";
    final String logout = "user/logout";
    final String userAll = "user/all";
    final String userDetail = "user/";
    final String userSave = "user/save";
    final String userUpdate = "user/update/";
    final String userSoftdelete = "user/softdelete/";
    final String userHarddelete = "user/harddelete/";

    final String merkAll = "barang/merk/all";
    final String merkDetail = "barang/merk/";
    final String merkSave = "barang/merk/save";
    final String merkUpdate = "barang/merk/update/";
    final String merkDelete = "barang/merk/delete/";

    final String jenisAll = "barang/jenis/all";
    final String jenisDetail = "barang/jenis/";
    final String jenisSave = "barang/jenis/save";
    final String jenisUpdate = "barang/jenis/update/";
    final String jenisDelete = "barang/jenis/delete/";

    final String barangAll = "barang/all";
    final String barangDetail = "barang/";
    final String barangSave = "barang/save";
    final String barangUpdate = "barang/update/";
    final String barangDelete = "barang/delete/";

    final String distributorSave = "distributor/save";
    final String distributor = "distributor/all";
    final String distributorDetail = "distributor";
    final String distributorStaff = "distributor/staff/";
    final String distributorUpdate = "distributor/update/";
    final String distributorSoftdelete = "distributor/softdelete/";
    final String distributorHarddelete = "distributor/harddelete/";

    final String saveTobasket = "transaksai/save/tobasket";
    final String transaksiSave = "transaksi/save/";
    final String transaksiUpdate = "transaksi/update/";
    final String transaksiDelete1 = "transaksi/delete/";
    final String transaksiDeletenomortransaksi = "transaksi/delete/all/";

    final String invoiceAll = "invoice/all/";
    final String invoiceDetail = "invoice/";
    final String invoiceUpdate = "invoice/update/";
    final String invoiceDelete = "invoice/delete/";

    final String receiveorderAll = "receiveorder/all";
    final String receiveorderDetail = "receiveorder/";
    final String receiveorderUpdate = "receiveorder/update";

    var log = Logger();

    Future<Object> userLogin(String? user, String? pass) async {
        final body = {
            "user" : user,
            "pass" : pass
        };

        final uri = Uri.http(url + login);
        final response = await http.post(
            uri, 
            body: jsonEncode(body),
            headers: {
              "Access-Control-Allow-Origin": "*",
              'Content-Type': 'application/json',
              // 'Accept': '*/*',
            }
        );
        log.d('api Username: $user, Password: $pass');
        // log.d('response $response.statusCode');
        return response;

        // final json = jsonDecode(response.body) as Map;
        // if(response.statusCode == 201) {
        //     return json['message'];
        // }
        // else {
        //     return json['message'];
        // }
    }

    Future<Object> getUserAll() async {
        final uri = Uri.http(url + userAll);
        final response = await http.get(uri);
        final json = jsonDecode(response.body) as Map;
        if(response.statusCode == 200) {
            return json['data'] as List;
        }
        else {
          return json['message'] as String;
        }
    }

    Future<Object> getBarangAll() async {
        final uri = Uri.parse(userAll);
        final response = await http.get(uri);
        final json = jsonDecode(response.body) as Map;
        if(response.statusCode == 200) {
            return json['data'] as List;
        }
        else {
          return json['message'] as String;
        }
    }

}