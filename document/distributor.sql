create database distributor;

--1
create table d0001_sistemotp(
	id_otp long(255) not null primary key auto_increment,
	id_user long(255) not null foreign key,
	kode_otp varchar(50) not null,
	expires_at date not null,
);

--2
create table d1001_user(
	id_user long(255) not null primary key auto_increment,
	username varchar(255) not null unique key,
	email varchar(255) not null unique key,
	tlp varchar(20) null unique key,
	password text null,
	roles int(5) not null, --- 0 = Tidak Ada; 1 = Super Admin; 2 = Admin Sistem; 3 = Admin Gudang; 4 = Admin Kawasan; 5 = Distributor; 6 = Staff Admin; 7 = Sales, dll;
	token varchar(100) null,
	status int(10) not null, -- 0 = inactive; 1 = updated; 2 = bought; 3 = pending, waiting for confirmation
	create_at date null,
	update_at date null
);

--3
create table d1002_userprofile(
	id_user long(255) not null primary key,
	fullname varchar(255) null,
	nickname varchar(255) null,
	jk enum('L','P') null,
	alamat text null,
	foto text null
);

--4
create table d1003_userwallet(
	id_user long(255) not null primary key,
	poin float(255) null,
	updated_at date null
);

--5
create table d1004_userpoinhistory(
	id_user long(255) not null primary key,
	deskripsi text null,
	status enum('poin up', 'claim') null,
	tgl_history date null
);

--6
create table d1005_persoalaccesstokens(
	id long(255) not null primary key auto_increment,
	tokenable_type varchar(255) not null,
	name varchar(255) not null,
	token varchar(255) not null,
	abilities varchar(255) not null,
	last_used_at datetime null,
	expires_at datetime null,
	created_at datetime null,
	updated_at datetime null,
);

--7
create table d2001_merkbarang(
	id_merkbarang long(255) not null primary key auto_increment,
	merk_barang varchar(255) not null,
	deskripsi text null
);

--8
create table d2002_jenisbarang(
	id_jenisbarang long(255) not null primary key auto_increment,
	jenis_barang varchar(255) not null
);

--9
create table d2003_barang(
	id_barang long(255) not null primary key auto_increment,
	nama_barang text null,
	jenis_barang long(255) not null foreign key,
	merk_barang long(255) not null foreign key,
	satuan varchar(255) not null,
	harga_beli float(255) null,
	harga_jual float(255) null,
	stok long(255) not null
);

-- FORMAT DI TAMPILAN SELECT
-- <select>
-- <option id={[COUNTER_ID-NAMA_BARANG-JENIS_BARANG-MERK_BARANG]} value={[ID_BARANG]}>{[MERK_BARANG] - [NAMA_BARANG]}</option>
-- </select>

--10
create table d3001_distributor(
	id_distributor varchar(255) not null primary key,
	id_user int(255) not null foreign key,
	nama varchar(255) not null,
	longitude float(255) null,
	latitude float(255) null
	created_at date null,
	updated_at date null
);

--11
create table d3002_distributorprofile(
	id_distributor varchar(255) not null primary key,
	deskripsi text null,
	alamat text null,
	foto text null
);

--12
create table d3003_distributorstaff(
	id_user long(255) not null primary key,
	id_distributor varchar(255) not null foreign key
	jabatan varchar(255) null
);

--13
create table d3004_distributorstaffposition(
	id_staffposition varchar(255) not null,
	id_distributor varchar(255) not null,
	jabatan varchar(255) not null,
);

--14
create table d4001_transaksi(
	id_transaksi varchar(255) not null primary key auto_increment,
	id_invoice long(255) not null foreign key,
	nomor_transaksi varchar(255) not null,
	barang int(255) not null foreign key,
	tujuan int(255) not null foreign key,
	jumlah_barang int(255) null,
	tgl_kirim date null
);

--15
create table d4002_invoice(
	id_invoice long(255) not null primary key auto_increment,
	tgl_invoice date not null	
);

--16
create table d4003_receiveorder(
	id_receiveorder long(255) not null primary key auto_increment,
	id_invoice long(255) not null foreign key,
	nomor_transaksi varchar(255) not null,
	barang long(255) not null foreign key,
	jumlah_barang long(255) null,
	tgl_sampai date null
);